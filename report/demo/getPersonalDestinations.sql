
-- les visites que login a le plus visité  entre currentTime - x et currentTime + x (exclure la salle ou il se trouve actuellement)

SELECT type_and_name
FROM public.visite
WHERE login_visiter = 'test' AND 
      type_and_name <> 'salle A462' AND
      heure >= 	current_time - interval '1 hour' AND --Server LocalTime (JAVA)
      heure <= 	current_time + interval '1 hour'
GROUP BY type_and_name
ORDER BY count(type_and_name) DESC;


-- les visites que login a le plus visité  en dehors de l'heure actuelle

SELECT type_and_name
FROM public.visite
WHERE login_visiter = 'test' AND 
      type_and_name <> 'salle A462' AND
      (heure <= current_time - interval '1 hour' OR
      heure >= current_time + interval '1 hour')
GROUP BY type_and_name
ORDER BY count(type_and_name) DESC;


-- les visites que les autres utilisateurs ont le plus visité entre currentTime - x et currentTime + x

SELECT type_and_name
FROM public.visite
WHERE login_visiter <> 'test' AND 
      type_and_name <> 'salle A462' AND
      heure >= 	current_time - interval '1 hour' AND
      heure <= 	current_time + interval '1 hour'
GROUP BY type_and_name
ORDER BY count(type_and_name) DESC;

-- les visites que login a le plus visité  en dehors de l'heure actuelle

SELECT type_and_name
FROM public.visite
WHERE login_visiter <> 'test' AND 
      type_and_name <> 'salle A462' AND
      (heure <= current_time OR
      heure >= current_time) 
GROUP BY type_and_name
ORDER BY count(type_and_name) DESC;