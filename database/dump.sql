--
-- PostgreSQL database dump
--

-- Dumped from database version 10.3
-- Dumped by pg_dump version 10.7

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


--
-- Name: equipmenttype; Type: TYPE; Schema: public; Owner: synthesis-project-m1
--

CREATE TYPE public.equipmenttype AS ENUM (
    'BEACON',
    'ACCESS_POINT'
);


ALTER TYPE public.equipmenttype OWNER TO "synthesis-project-m1";

--
-- Name: placetype; Type: TYPE; Schema: public; Owner: synthesis-project-m1
--

CREATE TYPE public.placetype AS ENUM (
    'salle',
    'restaurant',
    'cafeteria'
);


ALTER TYPE public.placetype OWNER TO "synthesis-project-m1";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: equipment; Type: TABLE; Schema: public; Owner: synthesis-project-m1
--

CREATE TABLE public.equipment (
    equipment_id character varying NOT NULL,
    place_id character varying(30) NOT NULL,
    longitude double precision NOT NULL,
    latitude double precision NOT NULL,
    type public.equipmenttype
);


ALTER TABLE public.equipment OWNER TO "synthesis-project-m1";

--
-- Name: beacon_id_beacon_seq; Type: SEQUENCE; Schema: public; Owner: synthesis-project-m1
--

CREATE SEQUENCE public.beacon_id_beacon_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.beacon_id_beacon_seq OWNER TO "synthesis-project-m1";

--
-- Name: beacon_id_beacon_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: synthesis-project-m1
--

ALTER SEQUENCE public.beacon_id_beacon_seq OWNED BY public.equipment.equipment_id;


--
-- Name: place; Type: TABLE; Schema: public; Owner: synthesis-project-m1
--

CREATE TABLE public.place (
    place_id character varying(30) NOT NULL,
    type text,
    geom public.geometry
);


ALTER TABLE public.place OWNER TO "synthesis-project-m1";

--
-- Name: user; Type: TABLE; Schema: public; Owner: synthesis-project-m1
--

CREATE TABLE public."user" (
    login character varying(30) NOT NULL,
    email character varying(30) NOT NULL,
    user_pwd character varying(60) NOT NULL
);


ALTER TABLE public."user" OWNER TO "synthesis-project-m1";

--
-- Name: visite; Type: TABLE; Schema: public; Owner: synthesis-project-m1
--

CREATE TABLE public.visite (
    visite_id integer NOT NULL,
    login_visiter character varying(30) NOT NULL,
    heure time without time zone NOT NULL,
    type_and_name character varying(30)
);


ALTER TABLE public.visite OWNER TO "synthesis-project-m1";

--
-- Name: visite_id_visite_seq; Type: SEQUENCE; Schema: public; Owner: synthesis-project-m1
--

CREATE SEQUENCE public.visite_id_visite_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.visite_id_visite_seq OWNER TO "synthesis-project-m1";

--
-- Name: visite_id_visite_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: synthesis-project-m1
--

ALTER SEQUENCE public.visite_id_visite_seq OWNED BY public.visite.visite_id;


--
-- Name: equipment equipment_id; Type: DEFAULT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.equipment ALTER COLUMN equipment_id SET DEFAULT nextval('public.beacon_id_beacon_seq'::regclass);


--
-- Name: visite visite_id; Type: DEFAULT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.visite ALTER COLUMN visite_id SET DEFAULT nextval('public.visite_id_visite_seq'::regclass);


--
-- Data for Name: equipment; Type: TABLE DATA; Schema: public; Owner: synthesis-project-m1
--

COPY public.equipment (equipment_id, place_id, longitude, latitude, type) FROM stdin;
\.


--
-- Data for Name: place; Type: TABLE DATA; Schema: public; Owner: synthesis-project-m1
--

COPY public.place (place_id, type, geom) FROM stdin;
A458	salle	01030000000100000006000000000000000000000000000000000000000000000000000000000000000000184033333333333317400000000000001C40000000000000184000000000000018400000000000001840000000000000000000000000000000000000000000000000
A462	salle	0103000000010000000600000000000000000018400000000000000000000000000000184000000000000018409A999999999927400000000000001C40000000000000284000000000000018400000000000002840000000000000000000000000000018400000000000000000
A464	salle	010300000001000000050000000000000000002840000000000000000000000000000028400000000000001440000000000000304000000000000014400000000000003040000000000000000000000000000028400000000000000000
A470-472	salle	010300000001000000050000000000000000003040000000000000000000000000000030400000000000001440000000000000384000000000000014400000000000003840000000000000000000000000000030400000000000000000
A488	salle	010300000001000000050000000000000000003840000000000000000000000000000038400000000000001440000000000000424000000000000014400000000000004240000000000000000000000000000038400000000000000000
AWC1	toilette femme	010300000001000000050000000000000000002E4000000000000014400000000000002E400000000000001C4000000000000032400000000000001C40000000000000324000000000000014400000000000002E400000000000001440
Aasc1	ascenseur	010300000001000000050000000000000000003640000000000000144000000000000036400000000000001C4000000000000038400000000000001C400000000000003840000000000000144000000000000036400000000000001440
A476.1	local	010300000001000000060000000000000000003840000000000000144000000000000038400000000000001C40CDCCCCCCCCCC39400000000000001C40CDCCCCCCCCCC39400000000000001C40CDCCCCCCCCCC3940000000000000144000000000000038400000000000001440
A451	salle	010300000001000000050000000000000000000000000000000000224000000000000000000000000000002840000000000000184000000000000028400000000000001840000000000000224000000000000000000000000000002240
A461	salle	010300000001000000050000000000000000002440000000000000224000000000000024400000000000002840000000000000284000000000000028400000000000002840000000000000224000000000000024400000000000002240
A463	salle	0103000000010000000500000000000000000028400000000000002240000000000000284000000000000028400000000000002C4000000000000028400000000000002C40000000000000224000000000000028400000000000002240
A465	salle	010300000001000000050000000000000000002C4000000000000022400000000000002C40000000000000284000000000000030400000000000002840000000000000304000000000000022400000000000002C400000000000002240
A469-473	salle	010300000001000000050000000000000000003240000000000000224000000000000032400000000000002840000000000000384000000000000028400000000000003840000000000000224000000000000032400000000000002240
A477	salle	0103000000010000000500000000000000000038400000000000002240000000000000384000000000000028400000000000003C4000000000000028400000000000003C40000000000000224000000000000038400000000000002240
A479	salle	010300000001000000050000000000000000003C4000000000000022400000000000003C4000000000000028400000000000003E4000000000000028400000000000003E4000000000000022400000000000003C400000000000002240
A481	salle	010300000001000000050000000000000000003E4000000000000022400000000000003E40000000000000284000000000000040400000000000002840000000000000404000000000000022400000000000003E400000000000002240
A483	salle	010300000001000000050000000000000000004040000000000000224000000000000040400000000000002840000000000000414000000000000028400000000000004140000000000000224000000000000040400000000000002240
A485	salle	010300000001000000050000000000000000004140000000000000224000000000000041400000000000002840000000000000424000000000000028400000000000004240000000000000224000000000000041400000000000002240
A487	salle	010300000001000000050000000000000000004240000000000000224000000000000042400000000000002840000000000000434000000000000028400000000000004340000000000000224000000000000042400000000000002240
A489	salle	010300000001000000050000000000000000004340000000000000224000000000000043400000000000002840000000000000444000000000000028400000000000004440000000000000224000000000000043400000000000002240
A4WC2	toilette homme	010300000001000000050000000000000000003E4000000000000014400000000000003E400000000000001C4000000000008040400000000000001C40000000000080404000000000000014400000000000003E400000000000001440
A498	salle	0103000000010000000B000000000000000000424000000000000000000000000000004240000000000000144000000000008040400000000000001440000000000080404033333333333319400000000000804140333333333333194000000000008041400000000000001C4000000000000047400000000000001C4000000000000047403333333333331740000000000000484033333333333317400000000000004840000000000000000000000000000042400000000000000000
A457	salle	010300000001000000050000000000000000001840000000000000224000000000000018400000000000002840000000000000244000000000000028400000000000002440000000000000224000000000000018400000000000002240
A467	salle	010300000001000000050000000000000000003040000000000000224000000000000030400000000000002840000000000000324000000000000028400000000000003240000000000000224000000000000030400000000000002240
A495-497	salle	010300000001000000050000000000000000004540000000000000224000000000000045400000000000002840000000000000484000000000000028400000000000004840000000000000224000000000000045400000000000002240
A491	salle	010300000001000000050000000000000000004440000000000000224000000000000044400000000000002840000000000000454000000000000028400000000000004540000000000000224000000000000044400000000000002240
A4couloir	couloir	0103000000010000001A000000000000000000000000000000000018400000000000000000000000000000224000000000000048400000000000002240000000000000484033333333333317400000000000004740333333333333174000000000000047400000000000001C4000000000008041400000000000001C40000000000080414033333333333319400000000000804040333333333333194000000000008040400000000000001C400000000000003E400000000000001C400000000000003E400000000000001440CDCCCCCCCCCC39400000000000001440CDCCCCCCCCCC39400000000000001C4000000000000036400000000000001C40000000000000364000000000000014400000000000003240000000000000144000000000000032400000000000001C400000000000002E400000000000001C400000000000002E40000000000000144000000000000028400000000000001440000000000000284000000000000018409A999999999927400000000000001C400000000000001840000000000000184033333333333317400000000000001C4000000000000000000000000000001840
\.


--
-- Data for Name: spatial_ref_sys; Type: TABLE DATA; Schema: public; Owner: _root
--

COPY public.spatial_ref_sys (srid, auth_name, auth_srid, srtext, proj4text) FROM stdin;
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: synthesis-project-m1
--

COPY public."user" (login, email, user_pwd) FROM stdin;
test	test@gmail.com	$1$rlzkMyKY$Gc.xhMjMx3FZIXZG6vtUJ1
aturing	aturing@gmail.com	$1$ZVUHh/M/$029nLTHG.jUCQ35O5kKe61
ladleman	ladleman@gmail.com	$1$tWAumVTc$6Dp1IOGJFK9kU8Snx1iQA0
rnixon	rnixon@gmail.com	$1$AwykeBPy$uTqRuPV2sqX7IRbMhOpmL0
htruman	htruman@gmail.com	$1$d31xW.84$H4Dk4X.Xaj.EZJb/R.et30
akerckhoffs	akerckhoffs@gmail.com	$1$sowgaJ1X$4fSzDQYEAWJ1eeUs7SMUW1
wdiffie	wdiffie@gmail.com	$1$iH6BEw3S$.k9kj4/o2EAe4SdlVqb9a1
gpompidou	gpompidou@gmail.com	$1$RxeTWXED$t4aKRpfkZvLH3hMSyachb.
cdegaulle	cdegaulle@gmail.com	$1$bY74FbI4$F8.1zWwCyAH3BNEgqx6Wd1
mrabin	mrabin@gmail.com	$1$6QTnpJ6q$r1p2d3sm3DHnSQ3LoL46e/
\.


--
-- Data for Name: visite; Type: TABLE DATA; Schema: public; Owner: synthesis-project-m1
--

COPY public.visite (visite_id, login_visiter, heure, type_and_name) FROM stdin;
94	test	14:36:51.117954	salle A458
95	test	14:36:56.184382	salle A458
96	test	14:37:01.298194	salle A458
97	test	14:37:06.311531	salle A458
98	test	14:37:11.55001	salle A458
99	test	14:37:17.621009	salle A458
100	test	14:37:22.591703	salle A458
101	test	14:37:27.714291	salle A458
102	test	14:37:33.248329	salle A458
103	test	14:37:38.528148	salle A458
104	test	14:37:43.665565	salle A458
105	test	14:37:50.21935	salle A458
106	test	14:37:54.851997	salle A458
107	test	14:37:59.701786	salle A458
108	test	14:38:06.11383	salle A458
109	test	14:38:10.70109	salle A458
110	test	14:38:15.860884	salle A458
111	test	14:38:21.595161	salle A458
112	test	14:38:26.560315	salle A458
113	test	14:38:32.310457	salle A458
114	test	14:38:37.307811	salle A458
41	test	13:30:00	salle A458
42	test	14:15:00	salle A458
43	test	14:10:00	salle A458
44	test	13:45:00	salle A462
45	test	13:40:00	salle A462
46	test	14:00:00	salle A464
47	test	08:30:00	salle A470-472
48	test	09:00:00	salle A470-472
49	test	09:30:00	salle A470-472
50	test	15:30:00	salle A488
51	test	16:30:00	salle A488
52	test	17:30:00	salle A498
53	aturing	13:30:00	sanitaire homme AWC1
54	aturing	14:00:00	sanitaire homme AWC1
55	mrabin	13:45:00	Locale de la rectangle A476.1
56	aturing	09:00:00	ascenseur asc1A
57	aturing	15:30:00	ascenseur asc1A
58	mrabin	16:30:00	sanitaire femme AWC2
115	test	14:38:42.990911	salle A458
116	test	14:38:51.030586	salle A458
117	test	14:38:56.189418	salle A458
118	test	14:39:01.358164	salle A458
119	test	14:39:06.448511	salle A458
120	test	14:39:11.671173	salle A458
121	test	14:39:17.128783	salle A458
122	test	14:39:22.347109	salle A458
123	test	14:39:27.350072	salle A458
124	test	14:39:32.443909	salle A458
125	test	14:39:37.667721	salle A458
126	test	14:39:43.872716	salle A458
127	test	14:39:51.295492	salle A458
128	test	14:40:03.92625	salle A458
129	test	14:40:52.482874	salle A458
130	test	14:40:57.352712	salle A458
131	test	14:41:02.409651	salle A458
132	test	14:41:07.552118	salle A458
133	test	14:41:12.606395	salle A458
134	test	14:41:19.267822	salle A458
135	test	14:41:24.636714	salle A458
136	test	14:41:29.708032	salle A458
\.


--
-- Name: beacon_id_beacon_seq; Type: SEQUENCE SET; Schema: public; Owner: synthesis-project-m1
--

SELECT pg_catalog.setval('public.beacon_id_beacon_seq', 1, false);


--
-- Name: visite_id_visite_seq; Type: SEQUENCE SET; Schema: public; Owner: synthesis-project-m1
--

SELECT pg_catalog.setval('public.visite_id_visite_seq', 136, true);


--
-- Name: equipment beacon_pkey; Type: CONSTRAINT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT beacon_pkey PRIMARY KEY (equipment_id);


--
-- Name: place salle_pkey; Type: CONSTRAINT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.place
    ADD CONSTRAINT salle_pkey PRIMARY KEY (place_id);


--
-- Name: user utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (login);


--
-- Name: visite visite_pkey; Type: CONSTRAINT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.visite
    ADD CONSTRAINT visite_pkey PRIMARY KEY (visite_id);


--
-- Name: equipment beacon_salle_fkey; Type: FK CONSTRAINT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.equipment
    ADD CONSTRAINT beacon_salle_fkey FOREIGN KEY (place_id) REFERENCES public.place(place_id);


--
-- Name: visite visite_id_utilisateur_fkey; Type: FK CONSTRAINT; Schema: public; Owner: synthesis-project-m1
--

ALTER TABLE ONLY public.visite
    ADD CONSTRAINT visite_id_utilisateur_fkey FOREIGN KEY (login_visiter) REFERENCES public."user"(login) ON DELETE CASCADE;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: synthesis-project-m1
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_out(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_out(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION box2d_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box2d_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION box2d_out(public.box2d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box2d_out(public.box2d) TO "synthesis-project-m1";


--
-- Name: FUNCTION box2df_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box2df_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION box2df_out(public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box2df_out(public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION box3d_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box3d_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION box3d_out(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box3d_out(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_analyze(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_analyze(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_in(cstring, oid, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_in(cstring, oid, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_out(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_out(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_recv(internal, oid, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_recv(internal, oid, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_send(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_send(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_typmod_in(cstring[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_typmod_in(cstring[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_typmod_out(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_typmod_out(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_analyze(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_analyze(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_out(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_out(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_recv(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_recv(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_send(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_send(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_typmod_in(cstring[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_typmod_in(cstring[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_typmod_out(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_typmod_out(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION gidx_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gidx_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION gidx_out(public.gidx); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gidx_out(public.gidx) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_abs_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_abs_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_abs_out(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_abs_out(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION spheroid_in(cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.spheroid_in(cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION spheroid_out(public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.spheroid_out(public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION __st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.__st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_overview_constraint(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, factor integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint(cn name, sql text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint(cn name, sql text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._add_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_overview_constraint(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_overview_constraint(ovschema name, ovtable name, ovcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint(rastschema name, rasttable name, cn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint(rastschema name, rasttable name, cn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_alignment(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_extent(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_nodata_values(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_num_bands(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_out_db(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_pixel_types(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._drop_raster_constraint_srid(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._overview_constraint(ov public.raster, factor integer, refschema name, reftable name, refcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._overview_constraint_info(ovschema name, ovtable name, ovcolumn name, OUT refschema name, OUT reftable name, OUT refcolumn name, OUT factor integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _postgis_deprecate(oldname text, newname text, version text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._postgis_deprecate(oldname text, newname text, version text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _postgis_join_selectivity(regclass, text, regclass, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._postgis_join_selectivity(regclass, text, regclass, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _postgis_pgsql_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._postgis_pgsql_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION _postgis_scripts_pgsql_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._postgis_scripts_pgsql_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION _postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._postgis_selectivity(tbl regclass, att_name text, geom public.geometry, mode text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _postgis_stats(tbl regclass, att_name text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._postgis_stats(tbl regclass, att_name text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_alignment(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_blocksize(rastschema name, rasttable name, rastcolumn name, axis text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_coverage_tile(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_extent(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_index(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_nodata_values(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_num_bands(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_out_db(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_pixel_types(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_regular_blocking(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_scale(rastschema name, rasttable name, rastcolumn name, axis character) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_spatially_unique(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_info_srid(rastschema name, rasttable name, rastcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_nodata_values(rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_nodata_values(rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_out_db(rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_out_db(rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION _raster_constraint_pixel_types(rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._raster_constraint_pixel_types(rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_3dintersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_asgeojson(integer, public.geography, integer, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geography, integer, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_asgeojson(integer, public.geometry, integer, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_asgeojson(integer, public.geometry, integer, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_asgml(integer, public.geography, integer, integer, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_asgml(integer, public.geography, integer, integer, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_asgml(integer, public.geometry, integer, integer, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_asgml(integer, public.geometry, integer, integer, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_askml(integer, public.geography, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_askml(integer, public.geography, integer, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_askml(integer, public.geometry, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_askml(integer, public.geometry, integer, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_aspect4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_asraster(geom public.geometry, scalex double precision, scaley double precision, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_asx3d(integer, public.geometry, integer, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_asx3d(integer, public.geometry, integer, integer, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_bestsrid(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_bestsrid(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_bestsrid(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_bestsrid(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_buffer(public.geometry, double precision, cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_buffer(public.geometry, double precision, cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_colormap(rast public.raster, nband integer, colormap text, method text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_colormap(rast public.raster, nband integer, colormap text, method text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_concavehull(param_inputgeom public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_concavehull(param_inputgeom public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_contains(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_containsproperly(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_convertarray4ma(value double precision[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_convertarray4ma(value double precision[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_count(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_countagg_finalfn(agg public.agg_count); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_countagg_finalfn(agg public.agg_count) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_countagg_transfn(agg public.agg_count, rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_coveredby(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_coveredby(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_covers(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_covers(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_covers(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_covers(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_crosses(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_crosses(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_distance(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_distance(public.geography, public.geography, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_distancetree(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_distancetree(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_distancetree(public.geography, public.geography, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_distanceuncached(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_distanceuncached(public.geography, public.geography, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dwithin(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dwithin(public.geography, public.geography, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dwithinuncached(public.geography, public.geography, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_dwithinuncached(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_dwithinuncached(public.geography, public.geography, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_equals(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_expand(public.geography, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_expand(public.geography, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_gdalwarp(rast public.raster, algorithm text, maxerr double precision, srid integer, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, width integer, height integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_geomfromgml(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_geomfromgml(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_hillshade4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, min double precision, max double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_intersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_intersects(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_intersects(geom public.geometry, rast public.raster, nband integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_intersects(geom public.geometry, rast public.raster, nband integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_longestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_longestline(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, distancex integer, distancey integer, extenttype text, customextent public.raster, mask double precision[], weighted boolean, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_maxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_orderingequals(geometrya public.geometry, geometryb public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_overlaps(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_pixelaspolygons(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_pointoutside(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_pointoutside(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_roughness4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_samealignment_finalfn(agg public.agg_samealignment); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_samealignment_finalfn(agg public.agg_samealignment) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_samealignment_transfn(agg public.agg_samealignment, rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_samealignment_transfn(agg public.agg_samealignment, rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], hasnosetvalue boolean, nosetvalue double precision, keepnodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_slope4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_summarystats_finalfn(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_summarystats_finalfn(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, boolean, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, boolean, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, integer, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_summarystats_transfn(internal, public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_summarystats_transfn(internal, public.raster, integer, boolean, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_tile(rast public.raster, width integer, height integer, nband integer[], padwithnodata boolean, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_touches(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_touches(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_tpi4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_tri4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_union_finalfn(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_union_finalfn(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, public.unionarg[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, public.unionarg[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_union_transfn(internal, public.raster, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_union_transfn(internal, public.raster, integer, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_voronoi(g1 public.geometry, clip public.geometry, tolerance double precision, return_polygons boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_within(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION _updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public._updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION addauth(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addauth(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying, new_srid integer, new_type character varying, new_dim integer, use_typmod boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer, new_type character varying, new_dim integer, use_typmod boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addoverviewconstraints(ovtable name, ovcolumn name, reftable name, refcolumn name, ovfactor integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addoverviewconstraints(ovschema name, ovtable name, ovcolumn name, refschema name, reftable name, refcolumn name, ovfactor integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.addrasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION armor(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.armor(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION armor(bytea, text[], text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.armor(bytea, text[], text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION box(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION box(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION box2d(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box2d(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION box2d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box2d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION box3d(public.box2d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box3d(public.box2d) TO "synthesis-project-m1";


--
-- Name: FUNCTION box3d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box3d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION box3d(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box3d(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION box3dtobox(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.box3dtobox(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION bytea(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.bytea(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION bytea(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.bytea(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION bytea(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.bytea(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION checkauth(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.checkauth(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION checkauth(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.checkauth(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION checkauthtrigger(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.checkauthtrigger() TO "synthesis-project-m1";


--
-- Name: FUNCTION contains_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION contains_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.contains_2d(public.box2df, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION contains_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.contains_2d(public.geometry, public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION crypt(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.crypt(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION dearmor(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dearmor(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION decrypt(bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.decrypt(bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION decrypt_iv(bytea, bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.decrypt_iv(bytea, bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION digest(bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.digest(bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION digest(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.digest(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION disablelongtransactions(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.disablelongtransactions() TO "synthesis-project-m1";


--
-- Name: FUNCTION dropgeometrycolumn(table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(table_name character varying, column_name character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(schema_name character varying, table_name character varying, column_name character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropgeometrycolumn(catalog_name character varying, schema_name character varying, table_name character varying, column_name character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropgeometrytable(table_name character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropgeometrytable(table_name character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropgeometrytable(schema_name character varying, table_name character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropgeometrytable(schema_name character varying, table_name character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropgeometrytable(catalog_name character varying, schema_name character varying, table_name character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropoverviewconstraints(ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovtable name, ovcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.dropoverviewconstraints(ovschema name, ovtable name, ovcolumn name) TO "synthesis-project-m1";


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, VARIADIC constraints text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, VARIADIC constraints text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.droprasterconstraints(rastschema name, rasttable name, rastcolumn name, srid boolean, scale_x boolean, scale_y boolean, blocksize_x boolean, blocksize_y boolean, same_alignment boolean, regular_blocking boolean, num_bands boolean, pixel_types boolean, nodata_values boolean, out_db boolean, extent boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION enablelongtransactions(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.enablelongtransactions() TO "synthesis-project-m1";


--
-- Name: FUNCTION encrypt(bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.encrypt(bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION encrypt_iv(bytea, bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.encrypt_iv(bytea, bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.equals(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION find_srid(character varying, character varying, character varying); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.find_srid(character varying, character varying, character varying) TO "synthesis-project-m1";


--
-- Name: FUNCTION gen_random_bytes(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gen_random_bytes(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION gen_random_uuid(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gen_random_uuid() TO "synthesis-project-m1";


--
-- Name: FUNCTION gen_salt(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gen_salt(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION gen_salt(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gen_salt(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geog_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geog_brin_inclusion_add_value(internal, internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography(public.geography, integer, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography(public.geography, integer, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_cmp(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_cmp(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_distance_knn(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_distance_knn(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_eq(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_eq(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_ge(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_ge(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_compress(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_compress(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_consistent(internal, public.geography, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_consistent(internal, public.geography, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_decompress(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_decompress(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_distance(internal, public.geography, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_distance(internal, public.geography, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_penalty(internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_penalty(internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_picksplit(internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_picksplit(internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_same(public.box2d, public.box2d, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_same(public.box2d, public.box2d, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gist_union(bytea, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gist_union(bytea, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_gt(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_gt(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_le(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_le(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_lt(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_lt(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geography_overlaps(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geography_overlaps(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geom2d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geom2d_brin_inclusion_add_value(internal, internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geom3d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geom3d_brin_inclusion_add_value(internal, internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geom4d_brin_inclusion_add_value(internal, internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geom4d_brin_inclusion_add_value(internal, internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(path); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(path) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(point); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(point) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(polygon); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(polygon) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(public.box2d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(public.box2d) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry(public.geometry, integer, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry(public.geometry, integer, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_above(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_above(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_below(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_below(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_cmp(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_cmp(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_contained_by_raster(public.geometry, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_contained_by_raster(public.geometry, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_contains(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_distance_box(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_distance_box(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_distance_centroid(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_distance_centroid_nd(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_distance_centroid_nd(public.geometry, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_distance_cpa(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_distance_cpa(public.geometry, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_eq(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_eq(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_ge(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_ge(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_compress_2d(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_compress_2d(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_compress_nd(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_compress_nd(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_consistent_2d(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_consistent_2d(internal, public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_consistent_nd(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_consistent_nd(internal, public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_decompress_2d(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_decompress_2d(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_decompress_nd(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_decompress_nd(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_distance_2d(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_distance_2d(internal, public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_distance_nd(internal, public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_distance_nd(internal, public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_penalty_2d(internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_penalty_2d(internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_penalty_nd(internal, internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_penalty_nd(internal, internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_picksplit_2d(internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_picksplit_2d(internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_picksplit_nd(internal, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_picksplit_nd(internal, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_same_2d(geom1 public.geometry, geom2 public.geometry, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_same_nd(public.geometry, public.geometry, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_same_nd(public.geometry, public.geometry, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_union_2d(bytea, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_union_2d(bytea, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gist_union_nd(bytea, internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gist_union_nd(bytea, internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_gt(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_gt(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_le(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_le(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_left(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_left(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_lt(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_lt(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_overabove(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_overabove(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_overbelow(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_overbelow(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_overlaps(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_overlaps_nd(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_overlaps_nd(public.geometry, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_overleft(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_overleft(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_overright(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_overright(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_raster_contain(public.geometry, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_raster_contain(public.geometry, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_raster_overlap(public.geometry, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_raster_overlap(public.geometry, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_right(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_right(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_same(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_same(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometry_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometry_within(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometrytype(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometrytype(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION geometrytype(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geometrytype(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION geomfromewkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geomfromewkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION geomfromewkt(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.geomfromewkt(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION get_proj4_from_srid(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.get_proj4_from_srid(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION gettransactionid(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gettransactionid() TO "synthesis-project-m1";


--
-- Name: FUNCTION gserialized_gist_joinsel_2d(internal, oid, internal, smallint); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_2d(internal, oid, internal, smallint) TO "synthesis-project-m1";


--
-- Name: FUNCTION gserialized_gist_joinsel_nd(internal, oid, internal, smallint); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gserialized_gist_joinsel_nd(internal, oid, internal, smallint) TO "synthesis-project-m1";


--
-- Name: FUNCTION gserialized_gist_sel_2d(internal, oid, internal, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gserialized_gist_sel_2d(internal, oid, internal, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION gserialized_gist_sel_nd(internal, oid, internal, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.gserialized_gist_sel_nd(internal, oid, internal, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION hmac(bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.hmac(bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION hmac(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.hmac(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION is_contained_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION is_contained_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.box2df, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION is_contained_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.is_contained_2d(public.geometry, public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION lockrow(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION lockrow(text, text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION lockrow(text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, timestamp without time zone) TO "synthesis-project-m1";


--
-- Name: FUNCTION lockrow(text, text, text, text, timestamp without time zone); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.lockrow(text, text, text, text, timestamp without time zone) TO "synthesis-project-m1";


--
-- Name: FUNCTION longtransactionsenabled(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.longtransactionsenabled() TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_2d(public.box2df, public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_2d(public.box2df, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.box2df, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_2d(public.geometry, public.box2df); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_2d(public.geometry, public.box2df) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_geog(public.geography, public.gidx); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.geography, public.gidx) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_geog(public.gidx, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_geog(public.gidx, public.gidx); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_geog(public.gidx, public.gidx) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_nd(public.geometry, public.gidx); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.geometry, public.gidx) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_nd(public.gidx, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION overlaps_nd(public.gidx, public.gidx); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.overlaps_nd(public.gidx, public.gidx) TO "synthesis-project-m1";


--
-- Name: FUNCTION path(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.path(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asgeobuf_finalfn(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asgeobuf_finalfn(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asgeobuf_transfn(internal, anyelement); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asgeobuf_transfn(internal, anyelement) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asgeobuf_transfn(internal, anyelement, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asgeobuf_transfn(internal, anyelement, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asmvt_finalfn(internal); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asmvt_finalfn(internal) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement, text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_asmvt_transfn(internal, anyelement, text, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_asmvt_transfn(internal, anyelement, text, integer, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_accum_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_accum_transfn(public.pgis_abs, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_accum_transfn(public.pgis_abs, public.geometry, double precision, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_clusterintersecting_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_clusterintersecting_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_clusterwithin_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_clusterwithin_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_collect_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_collect_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_makeline_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_makeline_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_polygonize_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_polygonize_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgis_geometry_union_finalfn(public.pgis_abs); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgis_geometry_union_finalfn(public.pgis_abs) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_armor_headers(text, OUT key text, OUT value text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_armor_headers(text, OUT key text, OUT value text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_key_id(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_key_id(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_decrypt(bytea, bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_decrypt(bytea, bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_decrypt(bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_decrypt(bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_decrypt(bytea, bytea, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_decrypt(bytea, bytea, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_decrypt_bytea(bytea, bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_decrypt_bytea(bytea, bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_decrypt_bytea(bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_decrypt_bytea(bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_decrypt_bytea(bytea, bytea, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_decrypt_bytea(bytea, bytea, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_encrypt(text, bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_encrypt(text, bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_encrypt(text, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_encrypt(text, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_encrypt_bytea(bytea, bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_encrypt_bytea(bytea, bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_pub_encrypt_bytea(bytea, bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_pub_encrypt_bytea(bytea, bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_decrypt(bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_decrypt(bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_decrypt(bytea, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_decrypt(bytea, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_decrypt_bytea(bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_decrypt_bytea(bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_decrypt_bytea(bytea, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_decrypt_bytea(bytea, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_encrypt(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_encrypt(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_encrypt(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_encrypt(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_encrypt_bytea(bytea, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_encrypt_bytea(bytea, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION pgp_sym_encrypt_bytea(bytea, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.pgp_sym_encrypt_bytea(bytea, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION point(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.point(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION polygon(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.polygon(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION populate_geometry_columns(use_typmod boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.populate_geometry_columns(use_typmod boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION populate_geometry_columns(tbl_oid oid, use_typmod boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.populate_geometry_columns(tbl_oid oid, use_typmod boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_addbbox(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_addbbox(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_cache_bbox(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_cache_bbox() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_constraint_dims(geomschema text, geomtable text, geomcolumn text) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_constraint_srid(geomschema text, geomtable text, geomcolumn text) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_constraint_type(geomschema text, geomtable text, geomcolumn text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_constraint_type(geomschema text, geomtable text, geomcolumn text) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_dropbbox(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_dropbbox(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_full_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_full_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_gdal_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_gdal_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_geos_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_geos_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_getbbox(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_getbbox(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_hasbbox(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_hasbbox(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_lib_build_date(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_lib_build_date() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_lib_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_lib_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_libjson_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_libjson_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_liblwgeom_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_liblwgeom_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_libprotobuf_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_libprotobuf_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_libxml_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_libxml_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_noop(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_noop(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_noop(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_noop(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_proj_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_proj_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_raster_lib_build_date(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_raster_lib_build_date() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_raster_lib_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_raster_lib_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_raster_scripts_installed(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_raster_scripts_installed() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_scripts_build_date(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_scripts_build_date() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_scripts_installed(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_scripts_installed() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_scripts_released(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_scripts_released() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_svn_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_svn_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_transform_geometry(public.geometry, text, text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_transform_geometry(public.geometry, text, text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_type_name(geomname character varying, coord_dimension integer, use_new_name boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_typmod_dims(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_typmod_dims(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_typmod_srid(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_typmod_srid(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_typmod_type(integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_typmod_type(integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION postgis_version(); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.postgis_version() TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_above(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_above(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_below(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_below(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_contain(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_contain(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_contained(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_contained(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_contained_by_geometry(public.raster, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_contained_by_geometry(public.raster, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_eq(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_eq(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_geometry_contain(public.raster, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_geometry_contain(public.raster, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_geometry_overlap(public.raster, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_geometry_overlap(public.raster, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_hash(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_hash(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_left(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_left(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_overabove(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_overabove(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_overbelow(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_overbelow(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_overlap(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_overlap(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_overleft(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_overleft(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_overright(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_overright(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_right(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_right(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION raster_same(public.raster, public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.raster_same(public.raster, public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dclosestpoint(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3ddfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3ddistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3ddistance(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3ddwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dintersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dintersects(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dlength(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dlength(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dlength_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dlength_spheroid(public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dlongestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dlongestline(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dmakebox(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dmakebox(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dmaxdistance(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dperimeter(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dperimeter(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dshortestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dshortestline(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(rast public.raster, addbandargset public.addbandarg[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, addbandargset public.addbandarg[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, pixeltype text, initialvalue double precision, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrasts public.raster[], fromband integer, torastindex integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(torast public.raster, fromrast public.raster, fromband integer, torastindex integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, outdbfile text, outdbindex integer[], nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, index integer, pixeltype text, initialvalue double precision, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addband(rast public.raster, outdbfile text, outdbindex integer[], index integer, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addmeasure(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addmeasure(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_addpoint(geom1 public.geometry, geom2 public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_addpoint(geom1 public.geometry, geom2 public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_affine(public.geometry, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rast public.raster, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rast public.raster, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxcount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxhistogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, sample_percent double precision, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, sample_percent double precision, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, sample_percent double precision, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, sample_percent double precision, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxquantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rast public.raster, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_approxsummarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, sample_percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_area(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_area(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_area(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_area(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_area(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_area(geog public.geography, use_spheroid boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_area2d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_area2d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asbinary(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asbinary(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asbinary(public.geography, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geography, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asbinary(public.geometry, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asbinary(public.geometry, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asbinary(public.raster, outasin boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asbinary(public.raster, outasin boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asencodedpolyline(geom public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asencodedpolyline(geom public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asewkb(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asewkb(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asewkb(public.geometry, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asewkb(public.geometry, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asewkt(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asewkt(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asewkt(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asewkt(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asewkt(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asewkt(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgdalraster(rast public.raster, format text, options text[], srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgdalraster(rast public.raster, format text, options text[], srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeojson(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeojson(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeojson(geog public.geography, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeojson(geom public.geometry, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geog public.geography, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeojson(gj_version integer, geom public.geometry, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgml(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgml(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgml(geog public.geography, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgml(geog public.geography, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgml(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgml(geom public.geometry, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgml(version integer, geog public.geography, maxdecimaldigits integer, options integer, nprefix text, id text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgml(version integer, geom public.geometry, maxdecimaldigits integer, options integer, nprefix text, id text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ashexewkb(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ashexewkb(public.geometry, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ashexewkb(public.geometry, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asjpeg(rast public.raster, options text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, options text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nbands integer[], options text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], options text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nbands integer[], quality integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nbands integer[], quality integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nband integer, options text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, options text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asjpeg(rast public.raster, nband integer, quality integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asjpeg(rast public.raster, nband integer, quality integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_askml(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_askml(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_askml(geog public.geography, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_askml(geog public.geography, maxdecimaldigits integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_askml(geom public.geometry, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_askml(geom public.geometry, maxdecimaldigits integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_askml(version integer, geog public.geography, maxdecimaldigits integer, nprefix text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_askml(version integer, geom public.geometry, maxdecimaldigits integer, nprefix text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aslatlontext(geom public.geometry, tmpl text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aslatlontext(geom public.geometry, tmpl text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asmvtgeom(geom public.geometry, bounds public.box2d, extent integer, buffer integer, clip_geom boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asmvtgeom(geom public.geometry, bounds public.box2d, extent integer, buffer integer, clip_geom boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, pixeltype text, units text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspect(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspng(rast public.raster, options text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, options text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspng(rast public.raster, nbands integer[], options text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], options text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspng(rast public.raster, nbands integer[], compression integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nbands integer[], compression integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspng(rast public.raster, nband integer, options text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, options text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_aspng(rast public.raster, nband integer, compression integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_aspng(rast public.raster, nband integer, compression integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text[], value double precision[], nodataval double precision[], touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, ref public.raster, pixeltype text, value double precision, nodataval double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, scalex double precision, scaley double precision, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text[], value double precision[], nodataval double precision[], upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text[], value double precision[], nodataval double precision[], skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, gridx double precision, gridy double precision, pixeltype text, value double precision, nodataval double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asraster(geom public.geometry, width integer, height integer, pixeltype text, value double precision, nodataval double precision, upperleftx double precision, upperlefty double precision, skewx double precision, skewy double precision, touched boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_assvg(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_assvg(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_assvg(geog public.geography, rel integer, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_assvg(geog public.geography, rel integer, maxdecimaldigits integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_assvg(geom public.geometry, rel integer, maxdecimaldigits integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astext(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astext(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astext(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astext(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astiff(rast public.raster, options text[], srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, options text[], srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astiff(rast public.raster, compression text, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, compression text, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astiff(rast public.raster, nbands integer[], options text[], srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], options text[], srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astiff(rast public.raster, nbands integer[], compression text, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astiff(rast public.raster, nbands integer[], compression text, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry, prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_astwkb(geom public.geometry[], ids bigint[], prec integer, prec_z integer, prec_m integer, with_sizes boolean, with_boxes boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asx3d(geom public.geometry, maxdecimaldigits integer, options integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_azimuth(geog1 public.geography, geog2 public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_azimuth(geog1 public.geography, geog2 public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_azimuth(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_azimuth(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_band(rast public.raster, nbands integer[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands integer[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_band(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nband integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_band(rast public.raster, nbands text, delimiter character); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_band(rast public.raster, nbands text, delimiter character) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandisnodata(rast public.raster, forcechecking boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, forcechecking boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandisnodata(rast public.raster, band integer, forcechecking boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandisnodata(rast public.raster, band integer, forcechecking boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer[], OUT bandnum integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandmetadata(rast public.raster, band integer, OUT pixeltype text, OUT nodatavalue double precision, OUT isoutdb boolean, OUT path text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandnodatavalue(rast public.raster, band integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandnodatavalue(rast public.raster, band integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandpath(rast public.raster, band integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandpath(rast public.raster, band integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bandpixeltype(rast public.raster, band integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bandpixeltype(rast public.raster, band integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bdmpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bdmpolyfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_bdpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_bdpolyfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_boundary(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_boundary(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_boundingdiagonal(geom public.geometry, fits boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_boundingdiagonal(geom public.geometry, fits boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_box2dfromgeohash(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_box2dfromgeohash(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(text, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(public.geography, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(text, double precision, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(text, double precision, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(text, double precision, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(public.geography, double precision, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(public.geography, double precision, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(public.geography, double precision, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buffer(public.geometry, double precision, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buffer(public.geometry, double precision, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_buildarea(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_buildarea(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_centroid(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_centroid(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_centroid(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_centroid(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_centroid(public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_centroid(public.geography, use_spheroid boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_cleangeometry(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_cleangeometry(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision[], crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, geom public.geometry, nodataval double precision, crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer[], geom public.geometry, nodataval double precision[], crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clip(rast public.raster, nband integer, geom public.geometry, nodataval double precision, crop boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clipbybox2d(geom public.geometry, box public.box2d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clipbybox2d(geom public.geometry, box public.box2d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_closestpoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_closestpoint(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_closestpointofapproach(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_closestpointofapproach(public.geometry, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clusterdbscan(public.geometry, eps double precision, minpoints integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clusterdbscan(public.geometry, eps double precision, minpoints integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clusterintersecting(public.geometry[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clusterkmeans(geom public.geometry, k integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clusterkmeans(geom public.geometry, k integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clusterwithin(public.geometry[], double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry[], double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_collect(public.geometry[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_collect(public.geometry[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_collect(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_collect(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_collectionextract(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_collectionextract(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_collectionhomogenize(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_collectionhomogenize(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_colormap(rast public.raster, colormap text, method text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, colormap text, method text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_colormap(rast public.raster, nband integer, colormap text, method text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_colormap(rast public.raster, nband integer, colormap text, method text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_combine_bbox(public.box2d, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_combine_bbox(public.box2d, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_combine_bbox(public.box3d, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_combine_bbox(public.box3d, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_combinebbox(public.box2d, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box2d, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_combinebbox(public.box3d, public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_combinebbox(public.box3d, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_combinebbox(public.box3d, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_concavehull(param_geom public.geometry, param_pctconvex double precision, param_allow_holes boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_contains(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_contains(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_contains(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_contains(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_containsproperly(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_containsproperly(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_containsproperly(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_containsproperly(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_convexhull(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_convexhull(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_convexhull(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_convexhull(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_coorddim(geometry public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_coorddim(geometry public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_count(rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_count(rast public.raster, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_count(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_count(rast public.raster, nband integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_count(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_coveredby(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_coveredby(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_coveredby(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_coveredby(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_coveredby(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_coveredby(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_coveredby(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_coveredby(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_covers(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_covers(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_covers(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_covers(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_covers(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_covers(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_covers(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_covers(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_cpawithin(public.geometry, public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_cpawithin(public.geometry, public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_createoverview(tab regclass, col name, factor integer, algo text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_createoverview(tab regclass, col name, factor integer, algo text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_crosses(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_crosses(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_curvetoline(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_curvetoline(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_curvetoline(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_curvetoline(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_curvetoline(geom public.geometry, tol double precision, toltype integer, flags integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_curvetoline(geom public.geometry, tol double precision, toltype integer, flags integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_delaunaytriangles(g1 public.geometry, tolerance double precision, flags integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dfullywithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, rast2 public.raster, distance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dfullywithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_difference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_difference(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dimension(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dimension(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_disjoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_disjoint(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_disjoint(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_disjoint(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distance(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distance(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distance(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distance(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distance(public.geography, public.geography, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distance(public.geography, public.geography, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distance_sphere(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distance_sphere(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distance_spheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distancecpa(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distancecpa(public.geometry, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distancesphere(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distancesphere(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distancespheroid(geom1 public.geometry, geom2 public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distinct4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_distinct4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dump(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dump(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dumpaspolygons(rast public.raster, band integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dumppoints(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dumppoints(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dumprings(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dumprings(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer[], exclude_nodata_value boolean, OUT nband integer, OUT valarray double precision[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dumpvalues(rast public.raster, nband integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dwithin(text, text, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dwithin(text, text, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dwithin(public.geography, public.geography, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dwithin(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, rast2 public.raster, distance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dwithin(public.geography, public.geography, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dwithin(public.geography, public.geography, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_dwithin(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, distance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_endpoint(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_endpoint(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_envelope(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_envelope(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_envelope(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_envelope(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_equals(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_equals(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_estimated_extent(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_estimated_extent(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_estimated_extent(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_estimated_extent(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_estimatedextent(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_estimatedextent(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_estimatedextent(text, text, text, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_estimatedextent(text, text, text, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_expand(public.box2d, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_expand(public.box2d, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_expand(public.box3d, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_expand(public.box3d, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_expand(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_expand(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_expand(box public.box2d, dx double precision, dy double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_expand(box public.box2d, dx double precision, dy double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_expand(box public.box3d, dx double precision, dy double precision, dz double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_expand(box public.box3d, dx double precision, dy double precision, dz double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_expand(geom public.geometry, dx double precision, dy double precision, dz double precision, dm double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_exteriorring(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_exteriorring(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_find_extent(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_find_extent(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_find_extent(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_find_extent(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_findextent(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_findextent(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_findextent(text, text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_findextent(text, text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_flipcoordinates(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_flipcoordinates(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force2d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force2d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force3d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force3d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force3dm(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force3dm(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force3dz(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force3dz(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force4d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force4d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force_2d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force_2d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force_3d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force_3d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force_3dm(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force_3dm(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force_3dz(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force_3dz(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force_4d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force_4d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_force_collection(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_force_collection(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcecollection(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcecollection(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcecurve(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcecurve(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcepolygonccw(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcepolygonccw(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcepolygoncw(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcepolygoncw(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcerhr(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcerhr(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcesfs(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_forcesfs(public.geometry, version text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_forcesfs(public.geometry, version text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_frechetdistance(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_frechetdistance(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_fromgdalraster(gdaldata bytea, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_fromgdalraster(gdaldata bytea, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT create_options text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_gdaldrivers(OUT idx integer, OUT short_name text, OUT long_name text, OUT create_options text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_generatepoints(area public.geometry, npoints numeric); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_generatepoints(area public.geometry, npoints numeric) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geogfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geogfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geogfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geogfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geographyfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geographyfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geohash(geog public.geography, maxchars integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geohash(geog public.geography, maxchars integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geohash(geom public.geometry, maxchars integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geohash(geom public.geometry, maxchars integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomcollfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomcollfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomcollfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomcollfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomcollfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomcollfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomcollfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geometricmedian(g public.geometry, tolerance double precision, max_iter integer, fail_if_not_converged boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geometryfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geometryfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geometryfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geometryfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geometryn(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geometryn(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geometrytype(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geometrytype(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromewkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromewkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromewkt(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromewkt(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromgeohash(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromgeohash(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromgeojson(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromgeojson(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromgml(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromgml(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromgml(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromgml(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromkml(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromkml(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromtwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromtwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geomfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geomfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_georeference(rast public.raster, format text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_georeference(rast public.raster, format text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_geotransform(public.raster, OUT imag double precision, OUT jmag double precision, OUT theta_i double precision, OUT theta_ij double precision, OUT xoffset double precision, OUT yoffset double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_gmltosql(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_gmltosql(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_gmltosql(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_gmltosql(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_hasarc(geometry public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_hasarc(geometry public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_hasnoband(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_hasnoband(rast public.raster, nband integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_hausdorffdistance(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_height(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_height(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_hillshade(rast public.raster, nband integer, customextent public.raster, pixeltype text, azimuth double precision, altitude double precision, max_bright double precision, scale double precision, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rast public.raster, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_histogram(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, bins integer, width double precision[], "right" boolean, OUT min double precision, OUT max double precision, OUT count bigint, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_interiorringn(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_interiorringn(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_interpolatepoint(line public.geometry, point public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_interpolatepoint(line public.geometry, point public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast public.raster, geomin public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, geomin public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(geomin public.geometry, rast public.raster, band integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(geomin public.geometry, rast public.raster, band integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast public.raster, band integer, geomin public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast public.raster, band integer, geomin public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, rast2 public.raster, returnband text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersection(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, returnband text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(public.geography, public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(public.geography, public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(geom public.geometry, rast public.raster, nband integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(geom public.geometry, rast public.raster, nband integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(rast public.raster, nband integer, geom public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, nband integer, geom public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(rast public.raster, geom public.geometry, nband integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(rast public.raster, geom public.geometry, nband integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_intersects(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_invdistweight4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isclosed(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isclosed(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_iscollection(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_iscollection(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_iscoveragetile(rast public.raster, coverage public.raster, tilewidth integer, tileheight integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isempty(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isempty(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isempty(rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isempty(rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ispolygonccw(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ispolygonccw(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ispolygoncw(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ispolygoncw(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isring(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isring(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_issimple(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_issimple(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvalid(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvalid(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvalid(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvalid(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvaliddetail(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvaliddetail(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvaliddetail(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvalidreason(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvalidreason(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvalidreason(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_isvalidtrajectory(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_isvalidtrajectory(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length(geog public.geography, use_spheroid boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length2d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length2d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length2d_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length2d_spheroid(public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length2dspheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length2dspheroid(public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_length_spheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_length_spheroid(public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_lengthspheroid(public.geometry, public.spheroid); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_lengthspheroid(public.geometry, public.spheroid) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_line_interpolate_point(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_line_interpolate_point(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_line_locate_point(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_line_locate_point(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_line_substring(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_line_substring(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linecrossingdirection(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linefromencodedpolyline(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linefromencodedpolyline(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linefrommultipoint(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linefrommultipoint(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linefromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linefromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linefromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linefromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linefromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linefromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linefromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linefromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_lineinterpolatepoint(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_lineinterpolatepoint(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linelocatepoint(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linelocatepoint(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linemerge(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linemerge(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linestringfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linestringfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linestringfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linesubstring(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linesubstring(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_linetocurve(geometry public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_linetocurve(geometry public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_locate_along_measure(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_locate_along_measure(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_locate_between_measures(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_locate_between_measures(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_locatealong(geometry public.geometry, measure double precision, leftrightoffset double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_locatebetween(geometry public.geometry, frommeasure double precision, tomeasure double precision, leftrightoffset double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_locatebetweenelevations(geometry public.geometry, fromelevation double precision, toelevation double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_longestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_longestline(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_m(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_m(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makebox2d(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makebox2d(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeemptycoverage(tilewidth integer, tileheight integer, width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeemptycoverage(tilewidth integer, tileheight integer, width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeemptyraster(rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, pixelsize double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeemptyraster(width integer, height integer, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeenvelope(double precision, double precision, double precision, double precision, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeenvelope(double precision, double precision, double precision, double precision, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeline(public.geometry[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeline(public.geometry[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeline(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makepoint(double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makepoint(double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makepoint(double precision, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makepointm(double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makepointm(double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makepolygon(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makepolygon(public.geometry, public.geometry[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makepolygon(public.geometry, public.geometry[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makevalid(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makevalid(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, pixeltype text, expression text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, pixeltype text, expression text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rastbandargset public.rastbandarg[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer[], callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, mask double precision[], weighted boolean, pixeltype text, extenttype text, customextent public.raster, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast public.raster, nband integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebra(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer, callbackfunc regprocedure, pixeltype text, extenttype text, customextent public.raster, distancex integer, distancey integer, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, pixeltype text, expression text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast public.raster, band integer, pixeltype text, expression text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, rast2 public.raster, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebraexpr(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, expression text, pixeltype text, extenttype text, nodata1expr text, nodata2expr text, nodatanodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, onerastuserfunc regprocedure, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, onerastuserfunc regprocedure, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast public.raster, band integer, pixeltype text, onerastuserfunc regprocedure, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, rast2 public.raster, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafct(rast1 public.raster, band1 integer, rast2 public.raster, band2 integer, tworastuserfunc regprocedure, pixeltype text, extenttype text, VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mapalgebrafctngb(rast public.raster, band integer, pixeltype text, ngbwidth integer, ngbheight integer, onerastngbuserfunc regprocedure, nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_max4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_max4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_maxdistance(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_maxdistance(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mean4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mean4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mem_size(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mem_size(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_memsize(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_memsize(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_memsize(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_memsize(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_metadata(rast public.raster, OUT upperleftx double precision, OUT upperlefty double precision, OUT width integer, OUT height integer, OUT scalex double precision, OUT scaley double precision, OUT skewx double precision, OUT skewy double precision, OUT srid integer, OUT numbands integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_min4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_min4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_minconvexhull(rast public.raster, nband integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_minconvexhull(rast public.raster, nband integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mindist4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_minimumboundingcircle(inputgeom public.geometry, segs_per_quarter integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_minimumboundingradius(public.geometry, OUT center public.geometry, OUT radius double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_minimumclearance(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_minimumclearance(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_minimumclearanceline(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_minimumclearanceline(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_minpossiblevalue(pixeltype text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_minpossiblevalue(pixeltype text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mlinefromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mlinefromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mlinefromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mlinefromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mlinefromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mlinefromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mlinefromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpointfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpointfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpointfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpointfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpointfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpointfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpolyfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpolyfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpolyfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpolyfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpolyfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_mpolyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_mpolyfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multi(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multi(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multilinefromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multilinefromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multilinestringfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multilinestringfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multilinestringfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipointfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipointfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipointfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipointfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipolyfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipolyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipolyfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipolygonfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_multipolygonfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_multipolygonfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ndims(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ndims(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, columnx integer, rowy integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_nearestvalue(rast public.raster, band integer, columnx integer, rowy integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, pt public.geometry, distancex integer, distancey integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_neighborhood(rast public.raster, band integer, columnx integer, rowy integer, distancex integer, distancey integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_node(g public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_node(g public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_normalize(geom public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_normalize(geom public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_notsamealignmentreason(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_notsamealignmentreason(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_npoints(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_npoints(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_nrings(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_nrings(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_numbands(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_numbands(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_numgeometries(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_numgeometries(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_numinteriorring(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_numinteriorring(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_numinteriorrings(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_numinteriorrings(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_numpatches(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_numpatches(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_numpoints(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_numpoints(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_offsetcurve(line public.geometry, distance double precision, params text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_offsetcurve(line public.geometry, distance double precision, params text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_orderingequals(geometrya public.geometry, geometryb public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_orderingequals(geometrya public.geometry, geometryb public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_overlaps(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_overlaps(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_overlaps(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_overlaps(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_patchn(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_patchn(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_perimeter(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_perimeter(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_perimeter(geog public.geography, use_spheroid boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_perimeter(geog public.geography, use_spheroid boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_perimeter2d(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_perimeter2d(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelascentroid(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelascentroid(rast public.raster, x integer, y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelascentroids(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelaspoint(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelaspoint(rast public.raster, x integer, y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelaspoints(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelaspolygon(rast public.raster, x integer, y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelaspolygon(rast public.raster, x integer, y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelaspolygons(rast public.raster, band integer, exclude_nodata_value boolean, OUT geom public.geometry, OUT val double precision, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelheight(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelheight(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision[], exclude_nodata_value boolean, OUT val double precision, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelofvalue(rast public.raster, nband integer, search double precision, exclude_nodata_value boolean, OUT x integer, OUT y integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pixelwidth(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pixelwidth(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_point(double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_point(double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_point_inside_circle(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_point_inside_circle(public.geometry, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointfromgeohash(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointfromgeohash(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointinsidecircle(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointinsidecircle(public.geometry, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointn(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointn(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_pointonsurface(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_pointonsurface(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_points(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_points(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polyfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polyfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polyfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polyfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polyfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polyfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polyfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygon(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygon(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygon(rast public.raster, band integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygon(rast public.raster, band integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygonfromtext(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygonfromtext(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygonfromtext(text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygonfromtext(text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygonfromwkb(bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygonfromwkb(bytea, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygonfromwkb(bytea, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygonize(public.geometry[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygonize(public.geometry[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_project(geog public.geography, distance double precision, azimuth double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_project(geog public.geography, distance double precision, azimuth double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, exclude_nodata_value boolean, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, exclude_nodata_value boolean, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rast public.raster, nband integer, exclude_nodata_value boolean, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantiles double precision[], OUT quantile double precision, OUT value double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_quantile(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, quantile double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_range4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_range4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoord(rast public.raster, columnx integer, rowy integer, OUT longitude double precision, OUT latitude double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rastertoworldcoordx(rast public.raster, xr integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rastertoworldcoordx(rast public.raster, xr integer, yr integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordx(rast public.raster, xr integer, yr integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rastertoworldcoordy(rast public.raster, yr integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, yr integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rastertoworldcoordy(rast public.raster, xr integer, yr integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rastertoworldcoordy(rast public.raster, xr integer, yr integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, VARIADIC reclassargset public.reclassarg[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_reclass(rast public.raster, reclassexpr text, pixeltype text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, reclassexpr text, pixeltype text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_reclass(rast public.raster, nband integer, reclassexpr text, pixeltype text, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_relate(geom1 public.geometry, geom2 public.geometry, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_relate(geom1 public.geometry, geom2 public.geometry, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_relatematch(text, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_relatematch(text, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_removepoint(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_removepoint(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_removerepeatedpoints(geom public.geometry, tolerance double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_removerepeatedpoints(geom public.geometry, tolerance double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, usescale boolean, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, ref public.raster, algorithm text, maxerr double precision, usescale boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, scalex double precision, scaley double precision, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resample(rast public.raster, width integer, height integer, gridx double precision, gridy double precision, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalexy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rescale(rast public.raster, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, percentwidth double precision, percentheight double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width integer, height integer, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_resize(rast public.raster, width text, height text, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewxy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_reskew(rast public.raster, skewx double precision, skewy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_retile(tab regclass, col name, ext public.geometry, sfx double precision, sfy double precision, tw integer, th integer, algo text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_reverse(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_reverse(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotate(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotate(public.geometry, double precision, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotate(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotate(public.geometry, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotatex(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotatex(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotatey(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotatey(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotatez(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotatez(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_rotation(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_rotation(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_roughness(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_samealignment(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_samealignment(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_samealignment(ulx1 double precision, uly1 double precision, scalex1 double precision, scaley1 double precision, skewx1 double precision, skewy1 double precision, ulx2 double precision, uly2 double precision, scalex2 double precision, scaley2 double precision, skewx2 double precision, skewy2 double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_scale(public.geometry, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_scale(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_scale(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_scale(public.geometry, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_scalex(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_scalex(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_scaley(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_scaley(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_segmentize(geog public.geography, max_segment_length double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_segmentize(geog public.geography, max_segment_length double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_segmentize(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_segmentize(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setbandisnodata(rast public.raster, band integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setbandisnodata(rast public.raster, band integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setbandnodatavalue(rast public.raster, nodatavalue double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, nodatavalue double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setbandnodatavalue(rast public.raster, band integer, nodatavalue double precision, forcechecking boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_seteffectivearea(public.geometry, double precision, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_seteffectivearea(public.geometry, double precision, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setgeoreference(rast public.raster, georef text, format text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, georef text, format text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setgeoreference(rast public.raster, upperleftx double precision, upperlefty double precision, scalex double precision, scaley double precision, skewx double precision, skewy double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setgeotransform(rast public.raster, imag double precision, jmag double precision, theta_i double precision, theta_ij double precision, xoffset double precision, yoffset double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setpoint(public.geometry, integer, public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setpoint(public.geometry, integer, public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setrotation(rast public.raster, rotation double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setrotation(rast public.raster, rotation double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setscale(rast public.raster, scale double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scale double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setscale(rast public.raster, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setscale(rast public.raster, scalex double precision, scaley double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setskew(rast public.raster, skew double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skew double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setskew(rast public.raster, skewx double precision, skewy double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setskew(rast public.raster, skewx double precision, skewy double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setsrid(geog public.geography, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setsrid(geog public.geography, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setsrid(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setsrid(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setsrid(rast public.raster, srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setsrid(rast public.raster, srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setupperleft(rast public.raster, upperleftx double precision, upperlefty double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalue(rast public.raster, geom public.geometry, newvalue double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, geom public.geometry, newvalue double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalue(rast public.raster, x integer, y integer, newvalue double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, x integer, y integer, newvalue double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, nband integer, geom public.geometry, newvalue double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalue(rast public.raster, band integer, x integer, y integer, newvalue double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, geomvalset public.geomval[], keepnodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], noset boolean[], keepnodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, newvalueset double precision[], nosetvalue double precision, keepnodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_setvalues(rast public.raster, nband integer, x integer, y integer, width integer, height integer, newvalue double precision, keepnodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_sharedpaths(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_sharedpaths(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_shift_longitude(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_shift_longitude(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_shiftlongitude(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_shiftlongitude(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_shortestline(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_shortestline(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_simplify(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_simplify(public.geometry, double precision, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_simplify(public.geometry, double precision, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_simplifypreservetopology(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_simplifypreservetopology(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_simplifyvw(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_simplifyvw(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_skewx(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_skewx(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_skewy(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_skewy(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_slope(rast public.raster, nband integer, customextent public.raster, pixeltype text, units text, scale double precision, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snap(geom1 public.geometry, geom2 public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snap(geom1 public.geometry, geom2 public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(public.geometry, double precision, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(geom1 public.geometry, geom2 public.geometry, double precision, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalexy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_snaptogrid(rast public.raster, gridx double precision, gridy double precision, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_split(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_split(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_srid(geog public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_srid(geog public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_srid(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_srid(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_srid(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_srid(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_startpoint(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_startpoint(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_stddev4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_stddev4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_subdivide(geom public.geometry, maxvertices integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_subdivide(geom public.geometry, maxvertices integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_sum4ma(value double precision[], pos integer[], VARIADIC userargs text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_sum4ma(matrix double precision[], nodatamode text, VARIADIC args text[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summary(public.geography); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summary(public.geography) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summary(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summary(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summary(rast public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summary(rast public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystats(rast public.raster, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystats(rast public.raster, nband integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystats(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_swapordinates(geom public.geometry, ords cstring); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_swapordinates(geom public.geometry, ords cstring) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_symdifference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_symdifference(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_symmetricdifference(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_symmetricdifference(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, width integer, height integer, padwithnodata boolean, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer[], width integer, height integer, padwithnodata boolean, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tile(rast public.raster, nband integer, width integer, height integer, padwithnodata boolean, nodataval double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_touches(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_touches(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_touches(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_touches(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tpi(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(public.geometry, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(public.geometry, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(geom public.geometry, to_proj text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, to_proj text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(geom public.geometry, from_proj text, to_srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(geom public.geometry, from_proj text, to_proj text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(geom public.geometry, from_proj text, to_proj text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, alignto public.raster, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalexy double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, scalex double precision, scaley double precision, algorithm text, maxerr double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transform(rast public.raster, srid integer, algorithm text, maxerr double precision, scalex double precision, scaley double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_translate(public.geometry, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_translate(public.geometry, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_translate(public.geometry, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_transscale(public.geometry, double precision, double precision, double precision, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_transscale(public.geometry, double precision, double precision, double precision, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, pixeltype text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_tri(rast public.raster, nband integer, customextent public.raster, pixeltype text, interpolate_nodata boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_unaryunion(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_unaryunion(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.geometry[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.geometry[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_upperleftx(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_upperleftx(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_upperlefty(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_upperlefty(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, pt public.geometry, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, x integer, y integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, pt public.geometry, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_value(rast public.raster, band integer, x integer, y integer, exclude_nodata_value boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rast public.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT count integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuecount(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rast public.raster, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalues double precision[], roundto double precision, OUT value double precision, OUT percent double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_valuepercent(rastertable text, rastercolumn text, nband integer, exclude_nodata_value boolean, searchvalue double precision, roundto double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_voronoilines(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_voronoipolygons(g1 public.geometry, tolerance double precision, extend_to public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_width(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_width(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_within(geom1 public.geometry, geom2 public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_within(geom1 public.geometry, geom2 public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_within(rast1 public.raster, rast2 public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, rast2 public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_within(rast1 public.raster, nband1 integer, rast2 public.raster, nband2 integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_wkbtosql(wkb bytea); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_wkbtosql(wkb bytea) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_wkttosql(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_wkttosql(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, pt public.geometry, OUT columnx integer, OUT rowy integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoord(rast public.raster, longitude double precision, latitude double precision, OUT columnx integer, OUT rowy integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, xw double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, pt public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, pt public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordx(rast public.raster, xw double precision, yw double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, yw double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, yw double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, pt public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, pt public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_worldtorastercoordy(rast public.raster, xw double precision, yw double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_wrapx(geom public.geometry, wrap double precision, move double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_wrapx(geom public.geometry, wrap double precision, move double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_x(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_x(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_xmax(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_xmax(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_xmin(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_xmin(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_y(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_y(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ymax(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ymax(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_ymin(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_ymin(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_z(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_z(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_zmax(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_zmax(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_zmflag(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_zmflag(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_zmin(public.box3d); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_zmin(public.box3d) TO "synthesis-project-m1";


--
-- Name: FUNCTION text(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.text(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION unlockrows(text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.unlockrows(text) TO "synthesis-project-m1";


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION updategeometrysrid(character varying, character varying, character varying, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.updategeometrysrid(character varying, character varying, character varying, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.updategeometrysrid(catalogn_name character varying, schema_name character varying, table_name character varying, column_name character varying, new_srid_in integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION updaterastersrid(table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.updaterastersrid(table_name name, column_name name, new_srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.updaterastersrid(schema_name name, table_name name, column_name name, new_srid integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_3dextent(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_3dextent(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_accum(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_accum(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeobuf(anyelement); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeobuf(anyelement) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asgeobuf(anyelement, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asgeobuf(anyelement, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asmvt(anyelement); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asmvt(anyelement, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asmvt(anyelement, text, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement, text, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_asmvt(anyelement, text, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_asmvt(anyelement, text, integer, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clusterintersecting(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clusterintersecting(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_clusterwithin(public.geometry, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_clusterwithin(public.geometry, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_collect(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_collect(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_countagg(public.raster, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_countagg(public.raster, integer, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_countagg(public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_countagg(public.raster, integer, boolean, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_extent(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_extent(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_makeline(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_makeline(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_memcollect(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_memcollect(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_memunion(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_memunion(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_polygonize(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_polygonize(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_samealignment(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_samealignment(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystatsagg(public.raster, boolean, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, boolean, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystatsagg(public.raster, integer, boolean); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_summarystatsagg(public.raster, integer, boolean, double precision); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_summarystatsagg(public.raster, integer, boolean, double precision) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.geometry); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.geometry) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.raster); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.raster) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.raster, integer); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.raster, integer) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.raster, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.raster, text) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.raster, public.unionarg[]); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.raster, public.unionarg[]) TO "synthesis-project-m1";


--
-- Name: FUNCTION st_union(public.raster, integer, text); Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON FUNCTION public.st_union(public.raster, integer, text) TO "synthesis-project-m1";


--
-- Name: TABLE geography_columns; Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON TABLE public.geography_columns TO "synthesis-project-m1";


--
-- Name: TABLE geometry_columns; Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON TABLE public.geometry_columns TO "synthesis-project-m1";


--
-- Name: TABLE raster_columns; Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON TABLE public.raster_columns TO "synthesis-project-m1";


--
-- Name: TABLE raster_overviews; Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON TABLE public.raster_overviews TO "synthesis-project-m1";


--
-- Name: TABLE spatial_ref_sys; Type: ACL; Schema: public; Owner: _root
--

GRANT ALL ON TABLE public.spatial_ref_sys TO "synthesis-project-m1";


--
-- PostgreSQL database dump complete
--

