package tests;

import entry.*;

import static org.junit.Assert.*;

import model.UserBuilder;
import org.junit.Test;

public class UserControlTest {

    @Test
    public void userExists() {

        UserControl userControl = new UserControl();
        assertEquals(true, userControl.userExists("mrabin", "pwdrabin1"));
    }

    @Test
    public void userCanRegister() {

        UserControl userControl = new UserControl();
        assertEquals(false, userControl.userCanRegister("mrabin"));
    }

    @Test
    public void userBuilder() {
        model.UserBuilder userBuilder = new UserBuilder();
        assertEquals("test", userBuilder.userBuilder("test","test@gmail.com","test").getLogin());
    }

}