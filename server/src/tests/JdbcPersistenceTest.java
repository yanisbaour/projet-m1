package tests;

import model.PositionUser;
import model.*;
import entry.UserControl;
import jdbc.*;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.*;


public class JdbcPersistenceTest {

    @Test
    public void insertUser() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        UserControl userControl = new UserControl();
        User user = new User("test","test@gmail.com","test") ;
        if (userControl.userCanRegister(user.getLogin()))
        assertEquals(0, jdbcPersistence.persistUser(user));
    }

    @Test
    public void insertVisite() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        UserControl userControl = new UserControl();

        Visite visite = new Visite();

        visite.setLoginVisiter("test");
        visite.setTypeAndName("Salle A462");
        visite.setHeure(LocalTime.now());

        System.out.println(visite.getHeure());
        System.out.println(visite.getTypeAndName());
        System.out.println(visite.getLoginVisiter());

        assertEquals(0, jdbcPersistence.persistVisite(visite));
    }

    @Test
    public void checkForPlace() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        PositionUser positionUser = new PositionUser();
        positionUser.setX(1);//8
        positionUser.setY(2);//2
        System.out.println(jdbcPersistence.checkForPlace(positionUser));
    }

    @Test
    public void gettPlaceUserVititeThisTime() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        assertEquals(2, jdbcPersistence.getPlaceUserVititeThisTime("test","salle A462").size());
    }

    @Test
    public void getPlaceUserVititeNotThisTime() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        assertEquals(4, jdbcPersistence.getPlaceUserVititeNotThisTime("test","salle A462").size());
    }

    @Test
    public void getPlaceOtherUsersVititeThisTime() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        assertEquals(1, jdbcPersistence.getPlaceOtherUsersVititeThisTime("test","salle A462").size());
    }

    @Test
    public void getPlaceOtherUsersVititeNotThisTime() {
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        assertEquals(4, jdbcPersistence.getPlaceOtherUsersVititeNotThisTime("test","salle A462").size());
    }


}