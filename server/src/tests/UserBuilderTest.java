package tests;

import model.UserBuilder;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserBuilderTest {

    @Test
    public void userBuilder(){ // convertion

        UserBuilder userBuilder = new UserBuilder();
        assertEquals("login", userBuilder.userBuilder("login","email","user_pwd").getLogin());
        assertEquals("email", userBuilder.userBuilder("login","email","user_pwd").getEmail());
        assertEquals("user_pwd", userBuilder.userBuilder("login","email", "user_pwd").getUser_pwd());
    }
}