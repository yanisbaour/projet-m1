package tests;

import jdbc.*;
import model.*;

import static org.junit.Assert.*;
import org.junit.Test;

public class JdbcQueryTest {

    @Test
    public void selectPlace() {
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        String query = "";
        assertEquals("A458", jdbcQuery.selectPlace(query).get(0).getPlace_id());
    }

    @Test
    public void gettPlaceUserVititeThisTime() {
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        assertEquals(2, jdbcQuery.getPlaceUserVititeThisTime("test","salle A462").size());
    }

    @Test
    public void selectUser() {
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        String query = "login = 'mrabin' AND user_pwd = crypt('pwdrabin1',user_pwd);";
        assertEquals("mrabin", jdbcQuery.selectUser(query).get(0).getLogin());
    }

    @Test
    public void insertUser() {
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        assertEquals(0, jdbcQuery.insertUser("test","test@gmail.com","test"));
    }

    @Test
    public void placeContainsPoint() {
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());//A462
        Point2D position = new Point2D();
        position.setX(7);//8
        position.setY(1);//2
        assertEquals(true, jdbcQuery.placeContainsPoint(position,"A462"));
    }

    @Test
    public void getEquipmentPosition() {
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        Point2D point2D = new Point2D();
        point2D = jdbcQuery.getEquipmentPosition ("TP-LINK_6E9CB4");
        assertEquals(0.5, jdbcQuery.getEquipmentPosition ("TP-LINK_6E9CB2").getX(),0);
    }



}