package entry;

import model.PositionUser;
import model.UserBuilder;
import model.Place;
import model.Point2D;
import model.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.ArrayList;

import model.Visite;
import jdbc.JdbcConnection;
import jdbc.JdbcPersistence;
import jdbc.JdbcQuery;

public class BusinessLayer {

    //ClientsConnection clientsConnection = new ClientsConnection();

    public BusinessLayer(){}

    public static void checkForAuthentification(PrintWriter flux_sortie,BufferedReader flux_entree)throws IOException{
        UserControl userControl = new UserControl();

        String login, password;
        if ((login = flux_entree.readLine()) != null) {
            if ((password = flux_entree.readLine()) != null) {
                if (userControl.userExists(login,password)){
                    flux_sortie.println("succes");
                }else{
                    flux_sortie.println("echec");
                }
            }
        }
    }

    public static void inscription(PrintWriter flux_sortie,BufferedReader flux_entree)throws IOException{
        UserControl userControl = new UserControl();
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        UserBuilder userBuilder = new UserBuilder();

        String login, email, password;
        if ((login = flux_entree.readLine()) != null) {
            if ((email = flux_entree.readLine()) != null) {
                if ((password = flux_entree.readLine()) != null) {
                    if (userControl.userCanRegister(login)){
                        User user = userBuilder.userBuilder(login,email,password);
                        jdbcPersistence.persistUser(user);
                        flux_sortie.println("succes");
                    }else{
                        flux_sortie.println("echec : exists");
                    }
                }
            }
        }
    }

    public static void getPlaces(PrintWriter flux_sortie,BufferedReader flux_entree)throws IOException{
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        ArrayList<Place> places = jdbcPersistence.getPlace();
        String reply;
        StringBuilder builder = new StringBuilder();

        builder.toString();
        for (Place place : places) {
            builder.append(place.getType().concat(place.getPlace_id()).concat(";"));
        }

        reply = builder.toString();
        flux_sortie.println(reply);
    }

    public static void localisation(PrintWriter flux_sortie,BufferedReader flux_entree) throws IOException{

        String login,reply;
        UserControl userControl = new UserControl();
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        PositionUser positionUser = new PositionUser();
        Visite visite = new Visite();
        int insersionTrue;

        Point2D equipmentPosition1,equipmentPosition2,equipmentPosition3;
        PositionUser userPosition = new PositionUser();
        String equipment1,equipment2,equipment3;
        double rssiEquipment1,rssiEquipment2,rssiEquipment3,distance1,distance2,distance3;
        double frequencyEquipment1,frequencyEquipment2,frequencyEquipment3;

        distance1 = Double.parseDouble(flux_entree.readLine());
        distance2 = Double.parseDouble(flux_entree.readLine());
        distance3 = Double.parseDouble(flux_entree.readLine());

        System.out.println("diastance TP-LINK_6E9CB4 : "+distance1);
        System.out.println("diastance TP-LINK_6E896C : "+distance2);
        System.out.println("diastance TP-LINK_6E894A : "+distance3);

        userPosition.setX(Double.parseDouble(flux_entree.readLine()));
        userPosition.setY(Double.parseDouble(flux_entree.readLine()));

        System.out.println("Position x : "+userPosition.getX());
        System.out.println("Position y : "+userPosition.getY());

        login = flux_entree.readLine();
        userPosition.setTime(LocalTime.now());

        System.out.println("userPosition "+userPosition.getX()+" "+userPosition.getY() + " " +userPosition.getTime());
        System.out.println("login : "+login);

        //check for place
        reply = jdbcPersistence.checkForPlace(userPosition);

        if (reply.length() != 0 && login.length() !=0){
            visite.setLoginVisiter(login);
            visite.setTypeAndName(reply);
            visite.setHeure(LocalTime.now());
            insersionTrue = jdbcPersistence.persistVisite(visite);
        }

        System.out.println("reply "+reply);
        //answer
        flux_sortie.println(reply);
    }

    public static void positionUser(PrintWriter flux_sortie,BufferedReader flux_entree) throws IOException{

        String login,place;
        UserControl userControl = new UserControl();
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        JdbcQuery jdbcQuery = new JdbcQuery(JdbcConnection.getConnection());
        PositionUser positionUser = new PositionUser();
        Visite visite = new Visite();

        Point2D equipmentPosition1,equipmentPosition2,equipmentPosition3;
        PositionUser userPosition = new PositionUser();
        String equipment1,equipment2,equipment3;
        int insersionTrue;

        userPosition.setX(Double.parseDouble(flux_entree.readLine()));
        userPosition.setY(Double.parseDouble(flux_entree.readLine()));

        login = flux_entree.readLine();
        userPosition.setTime(LocalTime.now());

        System.out.println("insert userPosition :: x= "+userPosition.getX()+" y= "+userPosition.getY() + " heure= " +userPosition.getTime()+" login ="+login);

        //check for place
        place = jdbcPersistence.checkForPlace(userPosition);

        if (place.length() != 0 && login.length() !=0){
            visite.setLoginVisiter(login);
            visite.setTypeAndName(place);
            visite.setHeure(LocalTime.now());
            insersionTrue = jdbcPersistence.persistVisite(visite);
        }
    }

    //return 10 result max
    public static void getPlacesIntintelligently(PrintWriter flux_sortie,BufferedReader flux_entree)throws IOException{
        String reply, user, place;
        JdbcPersistence jdbcPersistence = new JdbcPersistence();
        StringBuilder builder = new StringBuilder();

        builder.toString();

        user = flux_entree.readLine();
        place = flux_entree.readLine();

        ArrayList<Visite> visitesLevel1 = jdbcPersistence.getPlaceUserVititeThisTime(user,place);
        int i =0,j=0;
        if (!visitesLevel1.isEmpty()){
            j=0;
            while (i < 10 && j < visitesLevel1.size() ) {
                builder.append(visitesLevel1.get(j).getTypeAndName().concat(";"));
                i++;
                j++;
            }
        }

        if (visitesLevel1.size()<10){
            ArrayList<Visite> visitesLevel2 = jdbcPersistence.getPlaceUserVititeNotThisTime(user,place);

            if (!visitesLevel2.isEmpty()){
                j=0;
                while (i < 10 && j < visitesLevel2.size() ) {
                    if (! (visitesLevel1.contains(visitesLevel2.get(j)))){
                        builder.append(visitesLevel2.get(j).getTypeAndName().concat(";"));
                        i++;
                        j++;
                    }
                }
            }

            if (visitesLevel1.size() + visitesLevel2.size() < 10){
                ArrayList<Visite> visitesLevel3 = jdbcPersistence.getPlaceOtherUsersVititeThisTime(user,place);
                if (!visitesLevel3.isEmpty()){
                    j=0;
                    while (i < 10 && j < visitesLevel3.size() ) {
                        if (! (visitesLevel1.contains(visitesLevel3.get(j)) || visitesLevel2.contains(visitesLevel3.get(j)))){
                            builder.append(visitesLevel3.get(j).getTypeAndName().concat(";"));
                            i++;
                            j++;
                        }
                    }
                }

                if (visitesLevel1.size() + visitesLevel2.size() + visitesLevel3.size() < 10){
                    ArrayList<Visite> visitesLevel4 = jdbcPersistence.getPlaceOtherUsersVititeNotThisTime(user,place);
                    if (!visitesLevel4.isEmpty()){
                        j=0;
                        while (i < 10 && j < visitesLevel4.size() ) {
                            if (! (visitesLevel1.contains(visitesLevel4.get(j)) || visitesLevel2.contains(visitesLevel4.get(j))|| visitesLevel3.contains(visitesLevel4.get(j)))){
                                builder.append(visitesLevel4.get(j).getTypeAndName().concat(";"));
                                i++;
                                j++;
                            }
                        }
                    }
                }
            }

        }

        reply = builder.toString();
        flux_sortie.println(reply);
    }

}
