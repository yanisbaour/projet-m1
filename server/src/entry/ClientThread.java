package entry;
import java.io.*;
import java.net.Socket;

import static entry.BusinessLayer.*;


// For every client's connection we call this class
public class ClientThread extends Thread{
    private String clientName = null;
    private DataInputStream is = null;
    private PrintStream os = null;
    private Socket clientSocket = null;
    private final ClientThread[] threads;
    private int maxClientsCount;

    public ClientThread(Socket clientSocket, ClientThread[] threads) {
        this.clientSocket = clientSocket;
        this.threads = threads;
        maxClientsCount = threads.length;
        BusinessLayer businessLayer = new BusinessLayer();
    }

    public void run() {
        int maxClientsCount = this.maxClientsCount;
        ClientThread[] threads = this.threads;

        try {
            /*
             * Create input and output streams for this client.
             */
            is = new DataInputStream(clientSocket.getInputStream());
            os = new PrintStream(clientSocket.getOutputStream());

            PrintWriter flux_sortie = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader flux_entree = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));
            String name;

            /* Start the conversation. */
            while (true) {
                String line = is.readLine();
                if (line.startsWith("/quit")) {
                    break;
                }
                switch(line) {
                    case "authentification":
                        System.out.println("authentification");
                        checkForAuthentification(flux_sortie,flux_entree);
                        break;
                    case "inscription":
                        inscription(flux_sortie,flux_entree);
                        break;
                    case "places":
                        getPlaces(flux_sortie,flux_entree);
                    case "placesIntintelligently":
                        System.out.println("getPlacesIntintelligently");
                        getPlacesIntintelligently(flux_sortie,flux_entree);
                    case "localisation":
                        System.out.println("localisation");
                        localisation(flux_sortie,flux_entree);
                        break;
                    case "positionUser":
                        positionUser(flux_sortie,flux_entree);
                        break;
                    default:
                        break;
                }
            }

            /*
             * Clean up. Set the current thread variable to null so that a new client
             * could be accepted by the server.
             */
            synchronized (this) {
                for (int i = 0; i < maxClientsCount; i++) {
                    if (threads[i] == this) {
                        threads[i] = null;
                    }
                }
            }
            /*
             * Close the output stream, close the input stream, close the socket.
             */
            is.close();
            os.close();
            clientSocket.close();
        } catch (IOException e) {
        }
    }
}