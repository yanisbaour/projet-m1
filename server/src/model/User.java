package model;

public class User {
    private String login;
    private String email;
    private String user_pwd;

    public User() {}

    public User(String login, String email, String user_pwd) {
        this.login = login;
        this.email = email;
        this.user_pwd = user_pwd;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_pwd() {
        return user_pwd;
    }

    public void setUser_pwd(String user_pwd) {
        this.user_pwd = user_pwd;
    }



}
