package model;

import java.time.LocalTime;

public class PositionUser {
    private double x;
    private double y;
    private LocalTime time;

    public PositionUser() {}

    public PositionUser(double x, double y, LocalTime time) {
        super();
        this.x = x;
        this.y = y;
        this.time = time;
    }

    public double calculateDistanceFromUserToBeacon(double rSSI){
        double distance;
        float mP=-30;
        distance=Math.pow(10,((mP-rSSI)/(Math.pow(10,2)))) /* *(11/15) */;
        return distance;
    }

    public double  calculateDistanceFromUserToAccessPoint(double frequency, double signalLevel){
        double distance;
        distance = Math.pow(10,(((27.55-(20*Math.log10(frequency))+Math.abs(signalLevel))/20)));
        return distance;
    }


    public PositionUser calculatePositionUserCoordonates (model.Point2D equipmentPosition1, model.Point2D equipmentPosition2, Point2D equipmentPosition3, double distance1, double distance2, double distance3){
        double temp1;
        double temp2;

        double x;
        double y;

        double x1=equipmentPosition1.getX();
        double y1=equipmentPosition1.getY();

        double x2=equipmentPosition2.getX();
        double y2=equipmentPosition2.getY();

        double x3=equipmentPosition3.getX();
        double y3=equipmentPosition3.getY();

        temp1 = ((Math.pow(x3,2)) - (Math.pow(x2,2)) + (Math.pow(y3,2)) - (Math.pow(y2,2)) + (Math.pow(distance2,2)) - (Math.pow(distance3,2))) /2;

        temp2 = ((Math.pow(x1,2)) - (Math.pow(x2,2)) + (Math.pow(y1,2)) - (Math.pow(y2,2)) + (Math.pow(distance2,2)) - (Math.pow(distance1,2))) /2;

        y = ((temp2*(x2-x3)) - (temp1*(x2-x1))) / ( ((y1-y2) * (x2-x3)) -  ((y3-y2)*(x2-x1)));

        x = ((y * (y1-y2)) - temp2) / (x2-x1);

        LocalTime time = LocalTime.now();

        PositionUser user = new PositionUser(x,y,time);

        return user;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
