package model;

public class Equipment {
    private int equipment_id;
    private String place_id;
    private Point2D coordinates;

    public Equipment() {
        this.coordinates = new Point2D();
    }

    public Equipment(int equipment_id, String place_id, Point2D coordinate) {
        this.equipment_id = equipment_id;
        this.place_id = place_id;
        this.coordinates = new Point2D();
    }
}
