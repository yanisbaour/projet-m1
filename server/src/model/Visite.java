package model;

import java.time.LocalTime;

public class Visite {

    private String visite_id;
    private String loginVisiter;
    private String typeAndName;
    private LocalTime heure;

    public Visite(){}

    public Visite(String visite_id, String loginVisiter, String typeAndName, LocalTime heure) {
        this.visite_id = visite_id;
        this.loginVisiter = loginVisiter;
        this.typeAndName = typeAndName;
        this.heure = heure;
    }

    public String getVisite_id() {
        return visite_id;
    }

    public void setVisite_id(String visite_id) {
        this.visite_id = visite_id;
    }

    public String getLoginVisiter() {
        return loginVisiter;
    }

    public void setLoginVisiter(String loginVisiter) {
        this.loginVisiter = loginVisiter;
    }

    public String getTypeAndName() {
        return typeAndName;
    }

    public void setTypeAndName(String typeAndName) {
        this.typeAndName = typeAndName;
    }

    public LocalTime getHeure() {
        return heure;
    }

    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }
}
