package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class JdbcConnection {
    private static String host = "postgresql-synthesis-project-m1.alwaysdata.net";
    private static String base = "synthesis-project-m1_2019";
    private static String user = "synthesis-project-m1";
    private static String password = "thiziri18";
    private static int port = 5432;
    private static String url = "jdbc:postgresql://postgresql-synthesis-project-m1.alwaysdata.net:5432/synthesis-project-m1_2019";// + host + ":" + port + "/" + base;


    private static Connection connection;

    public static Connection getConnection() {
        if (connection == null) {
            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (Exception e) {
                System.err.println("Connection failed : " + e.getMessage());
            }
        }
        return connection;
    }




}
