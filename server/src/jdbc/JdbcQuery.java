package jdbc;

import model.*;

import java.sql.*;
import java.time.LocalTime;
import java.util.ArrayList;


public class JdbcQuery {
    private Connection connection;

    public JdbcQuery(Connection connection) {
        this.connection = connection;
    }

    // SELECT Place
    public ArrayList<Place> selectPlace(String where) {
        ArrayList<Place> places;
        places = new ArrayList<Place>();
        try {
            Place tmpPlace;
            String selectPlaceQuery;

            if (where.length() != 0) {
                selectPlaceQuery = "SELECT * FROM place " + where + ";";
            } else {
                selectPlaceQuery = "SELECT * FROM place;";
            }

            PreparedStatement preparedStatement = connection.prepareStatement(selectPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                tmpPlace = new Place();
                tmpPlace.setPlace_id(result.getString("place_id"));
                tmpPlace.setType(result.getString("type"));
                places.add(tmpPlace);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return places;
    }

    // SELECT User
    public ArrayList<User> selectUser(String where) {
        ArrayList<User> users;
        users = new ArrayList<User>();
        try {
            User tmpUser;
            String selectUserQuery;

            if (where.length() != 0) {
                selectUserQuery = "SELECT * FROM public.user WHERE " + where + ";";
            } else {
                selectUserQuery = "SELECT * FROM public.user";
            }

            PreparedStatement preparedStatement = connection.prepareStatement(selectUserQuery);
            ResultSet result = preparedStatement.executeQuery();
            while (result.next()) {

                tmpUser = new User();
                tmpUser.setLogin(result.getString("login"));
                tmpUser.setEmail(result.getString("email"));
                tmpUser.setUser_pwd(result.getString("user_pwd"));
                users.add(tmpUser);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return users;
    }

    // INSERT User
    public int insertUser(String login,String email,String user_pwd) {
        int id = 0;
        try {
            if (login.length() != 0) {
                String insertUserQuery = "INSERT INTO public.user VALUES ('"+login+"','"+email+"',crypt('"+user_pwd+"', gen_salt('md5')));";

                PreparedStatement preparedStatement = connection.prepareStatement(insertUserQuery);

                preparedStatement.executeUpdate();

                ResultSet keys = preparedStatement.getGeneratedKeys();
                if(keys.next()) {
                    id = keys.getInt(1);
                }
                preparedStatement.close();
            }

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return id;
    }

    // INSERT Visite
    public int insertVisite(String login, String place, String heure) {
        int id = 0;
        try {
            if (login.length() != 0) {
                String insertVisiteQuery = "INSERT INTO public.visite(login_visiter,type_and_name,heure) VALUES ('"+login+"','"+place+"','"+heure+"');";

                PreparedStatement preparedStatement = connection.prepareStatement(insertVisiteQuery);

                preparedStatement.executeUpdate();

                ResultSet keys = preparedStatement.getGeneratedKeys();
                if(keys.next()) {
                    id = keys.getInt(1);
                }
                preparedStatement.close();
            }

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return id;
    }

    // check for place
    public boolean placeContainsPoint(Point2D coordinates, String place_id) {

        ArrayList<Place> places;
        places =  new ArrayList<Place>();
        try {
            Place tmpPlace;
            String checkForPlaceQuery;

            checkForPlaceQuery = "SELECT place_id,type FROM place WHERE ST_Contains(geom,ST_Point("+coordinates.getX()+","+coordinates.getY()+")) AND place_id ='"+place_id+"' ;";

            PreparedStatement preparedStatement = connection.prepareStatement(checkForPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {

                tmpPlace = new Place();
                tmpPlace.setPlace_id(result.getString("place_id"));
                tmpPlace.setType(result.getString("type"));
                places.add(tmpPlace);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }

        return !places.isEmpty();
    }

    // check for place
    public Point2D getEquipmentPosition(String equipment_id) {

        Point2D coordinates = new Point2D();
        try {
            String checkForPlaceQuery;

            checkForPlaceQuery = "SELECT longitude,latitude FROM public.equipment WHERE equipment_id ='"+equipment_id+"';";

            PreparedStatement preparedStatement = connection.prepareStatement(checkForPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            if (result.next()) {
                coordinates.setX(result.getDouble("longitude"));
                coordinates.setY(result.getDouble("latitude"));
            }
            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }

        return coordinates;
    }

    // SELECT Place visited this time
    public ArrayList<Visite> getPlaceUserVititeThisTime(String user,String actual_place) {
        ArrayList<Visite> visites;
        visites = new ArrayList<Visite>();
        try {
            Visite tmpVisite;
            String selectPlaceQuery;

                selectPlaceQuery = "SELECT type_and_name FROM public.visite WHERE login_visiter = '"+user+
                        "' AND type_and_name <> '"+actual_place+"' AND heure >= time '"+LocalTime.now()+
                        "' - interval '1 hour' AND heure <= time '"+LocalTime.now()+
                        "' + interval '1 hour' GROUP BY type_and_name ORDER BY count(type_and_name) DESC;";

            PreparedStatement preparedStatement = connection.prepareStatement(selectPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                tmpVisite = new Visite();
                tmpVisite.setTypeAndName(result.getString("type_and_name"));
                visites.add(tmpVisite);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return visites;
    }

    // SELECT Place visited by user this time
    public ArrayList<Visite> getPlaceUserVititeNotThisTime(String user,String actual_place) {
        ArrayList<Visite> visites;
        visites = new ArrayList<Visite>();
        try {
            Visite tmpVisite;
            String selectPlaceQuery;

            selectPlaceQuery = "SELECT type_and_name FROM public.visite WHERE login_visiter = '"+user+
                    "' AND type_and_name <> '"+actual_place+"' AND (heure <= time '"+LocalTime.now()+
                    "' - interval '1 hour' OR heure >= time '"+LocalTime.now()+
                    "' + interval '1 hour') GROUP BY type_and_name ORDER BY count(type_and_name) DESC;";

            PreparedStatement preparedStatement = connection.prepareStatement(selectPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                tmpVisite = new Visite();
                tmpVisite.setTypeAndName(result.getString("type_and_name"));
                visites.add(tmpVisite);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return visites;
    }

    // SELECT Place visited by other users this time
    public ArrayList<Visite> getPlaceOtherUsersVititeThisTime(String user,String actual_place) {
        ArrayList<Visite> visites;
        visites = new ArrayList<Visite>();
        try {
            Visite tmpVisite;
            String selectPlaceQuery;

            selectPlaceQuery = "SELECT type_and_name FROM public.visite WHERE login_visiter <> '"+user+
                    "' AND type_and_name <> '"+actual_place+"' AND heure >= time '"+LocalTime.now()+
                    "' - interval '1 hour' AND heure <= time '"+LocalTime.now()+
                    "' + interval '1 hour' GROUP BY type_and_name ORDER BY count(type_and_name) DESC;";

            PreparedStatement preparedStatement = connection.prepareStatement(selectPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                tmpVisite = new Visite();
                tmpVisite.setTypeAndName(result.getString("type_and_name"));
                visites.add(tmpVisite);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return visites;
    }


    // SELECT Place visited by other users not this time
    public ArrayList<Visite> getPlaceOtherUsersVititeNotThisTime(String user,String actual_place) {
        ArrayList<Visite> visites;
        visites = new ArrayList<Visite>();
        try {
            Visite tmpVisite;
            String selectPlaceQuery;

            selectPlaceQuery = "SELECT type_and_name FROM public.visite WHERE login_visiter <> '"+user+
                    "' AND type_and_name <> '"+actual_place+"' AND (heure <= time '"+LocalTime.now()+
                    "' - interval '1 hour' OR heure >= time '"+LocalTime.now()+
                    "' + interval '1 hour') GROUP BY type_and_name ORDER BY count(type_and_name) DESC;";

            PreparedStatement preparedStatement = connection.prepareStatement(selectPlaceQuery);

            ResultSet result = preparedStatement.executeQuery();

            while (result.next()) {
                tmpVisite = new Visite();
                tmpVisite.setTypeAndName(result.getString("type_and_name"));
                visites.add(tmpVisite);
            }

            preparedStatement.close();

        } catch (SQLException se) {
            System.err.println(se.getMessage());
        }
        return visites;
    }


}
