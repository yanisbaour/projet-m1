package jdbc;

import model.PositionUser;
import model.*;
import java.util.ArrayList;

public class JdbcPersistence {

    private JdbcQuery query;

    public JdbcPersistence() {
        this.query = new JdbcQuery(JdbcConnection.getConnection());
    }

    // SELECT Place
    public ArrayList<Place> getPlace(String where) {
        return query.selectPlace(where);
    }

    // SELECT Place whithout conditions
    public ArrayList<Place> getPlace() {
        return query.selectPlace("");
    }

    // SELECT places user visited this time
    public ArrayList<Visite> getPlaceUserVititeThisTime(String user,String actual_place) {
        return query.getPlaceUserVititeThisTime(user,actual_place);
    }

    // SELECT Place user visited but not this time
    public ArrayList<Visite> getPlaceUserVititeNotThisTime(String user,String actual_place) {
        return query.getPlaceUserVititeNotThisTime(user,actual_place);
    }

    // SELECT Place visited by other users this time
    public ArrayList<Visite> getPlaceOtherUsersVititeThisTime(String user,String actual_place) {
        return query.getPlaceOtherUsersVititeThisTime(user,actual_place);
    }

    // SELECT Place visited by other users not this time
    public ArrayList<Visite> getPlaceOtherUsersVititeNotThisTime(String user,String actual_place) {
        return query.getPlaceOtherUsersVititeNotThisTime(user,actual_place);
    }

    // SELECT User
    public ArrayList<User> getUser(String where) {
        return query.selectUser(where);
    }

    // SELECT User
    public ArrayList<User> getUser() {return query.selectUser("");}

    // INSERT User
    public int persistUser(User user) {
        return query.insertUser(user.getLogin(),user.getEmail(),user.getUser_pwd());
    }

    // INSERT visite
    public int persistVisite(Visite visite) {
        return (int) query.insertVisite(visite.getLoginVisiter() ,visite.getTypeAndName(),visite.getHeure().toString());
    }

    // CHECK FOR PLACE
    public String checkForPlace(PositionUser userPosition) {

        Point2D position = new Point2D();
        position.setX(userPosition.getX());
        position.setY(userPosition.getY());
        ArrayList<Place> places = getPlace("");
        for (Place place : places) {
            if (query.placeContainsPoint(position, place.getPlace_id())) {
                return place.getType()+" "+ place.getPlace_id();
            }
        }
        return "";
    }
}
