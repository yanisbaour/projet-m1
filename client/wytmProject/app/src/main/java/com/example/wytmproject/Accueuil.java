package com.example.wytmproject;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static java.lang.Thread.sleep;

public class Accueuil extends AppCompatActivity  {


    private WifiManager wifiManager;
    private final int MY_PERMISSIONS_ACCESS_COARSE_LOCATION = 1;
    WifiReceiver receiverWifi;
    TextView resultat;
    ArrayList<AccessPoint> accessPointList = new ArrayList<>();
    BusinessLayer businessLayer = new BusinessLayer();

    double distance1 =0;
    double distance2 =0;
    double distance3 =0;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueuil);
        Button buttonScan = findViewById(R.id.locate);
        Button destinations = findViewById(R.id.choixDEST);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled()) {
            Toast.makeText(getApplicationContext(), "Turning WiFi ON...", Toast.LENGTH_LONG).show();
            wifiManager.setWifiEnabled(true);
        }

        // send position to the server every 2 secondes
        handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //wifiManager.startScan();
                Intent intent = getIntent();
                String login =  intent.getStringExtra("login");

                PositionUser positionUser = new PositionUser();
                wifiManager.startScan();
                accessPointList.clear();
                accessPointList = receiverWifi.getAccessPoints();
                if (accessPointList.size()>=3){
                    distance1 = positionUser.calculateDistanceFromUserToAccessPoint(accessPointList.get(0).getAccessPoint_FREQUENCY(),accessPointList.get(0).getAccessPoint_RSSI(),accessPointList.get(0).getAccessPoint_SSID());
                    distance2 = positionUser.calculateDistanceFromUserToAccessPoint(accessPointList.get(1).getAccessPoint_FREQUENCY(),accessPointList.get(1).getAccessPoint_RSSI(),accessPointList.get(1).getAccessPoint_SSID());
                    distance3 = positionUser.calculateDistanceFromUserToAccessPoint(accessPointList.get(2).getAccessPoint_FREQUENCY(),accessPointList.get(2).getAccessPoint_RSSI(),accessPointList.get(2).getAccessPoint_SSID());

                    positionUser = positionUser.calculatePositionUserCoordonates (accessPointList.get(0).getAccessPoint_SSID(),accessPointList.get(1).getAccessPoint_SSID(),accessPointList.get(2).getAccessPoint_SSID(),distance1,distance2,distance3);
                    //businessLayer.sendPosition(positionUser.getX(),positionUser.getY(),login);
                    businessLayer.sendPosition(1,2,"test");
                }
                handler.postDelayed(this,5000);
            }
        };

        handler.postDelayed(runnable,5000);


        //pour le calcul de la moyenne
        final int testNumber = 1000;

        destinations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                String login =  intent.getStringExtra("login");
                if (ActivityCompat.checkSelfPermission(Accueuil.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            Accueuil.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_COARSE_LOCATION);
                } else {
                    Intent i = new Intent().setClass(Accueuil.this, DestinationListe.class);
                    i.putExtra("login", login);
                    startActivity(i);
                }
            }
        });


        buttonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login;

                PositionUser positionUser = new PositionUser();
                if (ActivityCompat.checkSelfPermission(Accueuil.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            Accueuil.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, MY_PERMISSIONS_ACCESS_COARSE_LOCATION);
                } else {
                    wifiManager.startScan();
                    resultat= findViewById(R.id.result);

                    //recuperate login from authentification
                    Intent intent = getIntent();
                    login =  intent.getStringExtra("login");

                    for (int i =0;i<testNumber;i++){
                        accessPointList.clear();
                        try {
                            sleep(1) ;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        accessPointList = receiverWifi.getAccessPoints();
                             if (accessPointList.size()>=3){
                                    distance1 = distance1 + positionUser.calculateDistanceFromUserToAccessPoint(accessPointList.get(0).getAccessPoint_FREQUENCY(),accessPointList.get(0).getAccessPoint_RSSI(),accessPointList.get(0).getAccessPoint_SSID());
                                    distance2 = distance2 + positionUser.calculateDistanceFromUserToAccessPoint(accessPointList.get(1).getAccessPoint_FREQUENCY(),accessPointList.get(1).getAccessPoint_RSSI(),accessPointList.get(1).getAccessPoint_SSID());
                                    distance3 = distance3 + positionUser.calculateDistanceFromUserToAccessPoint(accessPointList.get(2).getAccessPoint_FREQUENCY(),accessPointList.get(2).getAccessPoint_RSSI(),accessPointList.get(2).getAccessPoint_SSID());
                             }
                        }

                    if (accessPointList.size()>=3){
                        System.err.println("******************************************************************************* distance ");
                        System.out.println("distance to "+accessPointList.get(0).getAccessPoint_SSID() + " with RSSI "+accessPointList.get(0).getAccessPoint_RSSI() + " : "+distance1/testNumber);
                        System.out.println("distance to "+accessPointList.get(1).getAccessPoint_SSID() + " with RSSI "+accessPointList.get(1).getAccessPoint_RSSI() + " : "+distance2/testNumber);
                        System.out.println("distance to "+accessPointList.get(2).getAccessPoint_SSID() + " with RSSI "+accessPointList.get(2).getAccessPoint_RSSI() + " : "+distance3/testNumber);
                        System.err.println("******************************************************************************* position ");

                        positionUser = positionUser.calculatePositionUserCoordonates(accessPointList.get(0).getAccessPoint_SSID(),accessPointList.get(1).getAccessPoint_SSID(),accessPointList.get(2).getAccessPoint_SSID(),distance1/testNumber,distance2/testNumber,distance3/testNumber);

                        System.out.println("position : x = "+positionUser.getX() + "  y = "+positionUser.getY() );
                        System.err.println("*******************************************************************************");
                        resultat.setText(businessLayer.localisation(accessPointList.get(0).getAccessPoint_SSID(),accessPointList.get(1).getAccessPoint_SSID(),accessPointList.get(2).getAccessPoint_SSID(),distance1/testNumber,distance2/testNumber,distance3/testNumber,login));
                    }
                    distance1=0;
                    distance2=0;
                    distance3=0;
                }
            }

        });
    }
    @Override
    protected void onPostResume() {
        super.onPostResume();
        receiverWifi = new WifiReceiver(wifiManager);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        registerReceiver(receiverWifi, intentFilter);
        getWifi();
    }
    private void getWifi() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Toast.makeText(Accueuil.this, "version> = marshmallow", Toast.LENGTH_SHORT).show();
            if (ContextCompat.checkSelfPermission(Accueuil.this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                //Toast.makeText(Accueuil.this, "location turned off", Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(Accueuil.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_ACCESS_COARSE_LOCATION);
            } else {
                //Toast.makeText(Accueuil.this, "location turned on", Toast.LENGTH_SHORT).show();
                wifiManager.startScan();
            }
        } else {
            Toast.makeText(Accueuil.this, "scanning", Toast.LENGTH_SHORT).show();
            wifiManager.startScan();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiverWifi);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(Accueuil.this, "permission granted", Toast.LENGTH_SHORT).show();
                    wifiManager.startScan();
                } else {
                    Toast.makeText(Accueuil.this, "permission not granted", Toast.LENGTH_SHORT).show();
                    return;
                }
                break;
        }
    }
}



