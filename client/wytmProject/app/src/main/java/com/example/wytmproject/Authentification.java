package com.example.wytmproject;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.Objects;

public class Authentification extends AppCompatActivity {
    Button inscriptionBTN;
    TextView creerCompteInscriptionTV,recuperationMot2PassTV;
    private EditText identifiant, motdePass;

    BusinessLayer businessLayer = new BusinessLayer();

    public EditText getIdentifiant() {
        return identifiant;
    }

    public Authentification(){
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);

        identifiant = findViewById(R.id.username);
        motdePass = findViewById(R.id.pwd);
        inscriptionBTN = findViewById(R.id.login);


        inscriptionBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (android.os.Build.VERSION.SDK_INT > 9) { //ici la resolution du probleme
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }

                String chaineUserName = identifiant.getText().toString().toLowerCase();
                String chainePassword = motdePass.getText().toString().toLowerCase();

                if (businessLayer.checkForAuthentification(chaineUserName, chainePassword).equals("succes") )
                {
                    Intent i = new Intent().setClass(Authentification.this, Accueuil.class);
                    i.putExtra("login", chaineUserName);
                   startActivity(i);
                }else{
                   Toast.makeText(Authentification.this, "echec", Toast.LENGTH_SHORT).show();
                }
            }

        });

        creerCompteInscriptionTV = (TextView) findViewById(R.id.creationCompteInscription);
        creerCompteInscriptionTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2 = new Intent().setClass(Authentification.this,Inscription.class);
                startActivity(i2);
            }
        });
    }
}
