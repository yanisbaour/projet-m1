package com.example.wytmproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RecuperationMot2Pass extends AppCompatActivity {

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    EditText emailUser;
    Button btnEmailRecuperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperation_mot2_pass);

        btnEmailRecuperation = findViewById(R.id.submitRecuperation);
        emailUser = findViewById(R.id.emailRecuperation);
        btnEmailRecuperation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (emailUser.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"enter email address",Toast.LENGTH_SHORT).show();
                }else{
                    if (emailUser.getText().toString().trim().matches(emailPattern)){
                        Toast.makeText(getApplicationContext(),"valide email address",Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getApplicationContext(),"Invalid email address", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

//    private void alertDisplayer(String title,String message, final boolean error){
//        AlertDialog.Builder builder = new AlertDialog.Builder(RecuperationMot2Pass.this)
//                .setTitle(title)
//                .setMessage(message)
//                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                                if(!error) {
//                                    Intent intent = new Intent(RecuperationMot2Pass.this, RecuperationMot2Pass.class);
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(intent);
//                            }
//                        }
//                });
//                        AlertDialog ok = builder.create();
//        ok.show();
//    }
}

