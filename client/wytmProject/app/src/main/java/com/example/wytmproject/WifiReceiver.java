package com.example.wytmproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class WifiReceiver extends BroadcastReceiver {
    WifiManager wifiManager;
    StringBuilder sb;
    ArrayList<AccessPoint> accessPointList = new ArrayList<>();
    AccessPoint accessPoint1 = new AccessPoint(0,0);
    AccessPoint accessPoint2 = new AccessPoint(0,0);
    AccessPoint accessPoint3 = new AccessPoint(0,0);
    AccessPoint accessPoint4 = new AccessPoint(0,0);
    AccessPoint accessPoint5 = new AccessPoint(0,0);
    AccessPoint accessPoint6 = new AccessPoint(0,0);


    public WifiReceiver(WifiManager wifiManager){
        this.wifiManager = wifiManager;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(action)) {
            sb = new StringBuilder();
            accessPointList.clear();
            List<ScanResult> wifiList = wifiManager.getScanResults();

            int i=0;
            for (ScanResult scanResult : wifiList) {
                if (scanResult.SSID.contains("TP-LINK_6E896C")){

                    if (accessPoint1.getAccessPoint_RSSI()!=0){
                        accessPoint1.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint1.setAccessPoint_RSSI(0.5*accessPoint1.getAccessPoint_RSSI()+(0.5)*scanResult.level);
                        accessPoint1.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }else{
                        accessPoint1.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint1.setAccessPoint_RSSI(scanResult.level);
                        accessPoint1.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }
                }else if (scanResult.SSID.contains("TP-LINK_6E9CB2")){
                    if (accessPoint2.getAccessPoint_RSSI()!=0){
                        accessPoint2.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint2.setAccessPoint_RSSI(0.5*accessPoint2.getAccessPoint_RSSI()+(0.5)*scanResult.level);
                        accessPoint2.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }else{
                        accessPoint2.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint2.setAccessPoint_RSSI(scanResult.level);
                        accessPoint2.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }
                }else if (scanResult.SSID.contains("TP-LINK_44D2C4")){
                    if (accessPoint3.getAccessPoint_RSSI()!=0){
                        accessPoint3.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint3.setAccessPoint_RSSI(0.5*accessPoint3.getAccessPoint_RSSI()+(0.5)*scanResult.level);
                        accessPoint3.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }else{
                        accessPoint3.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint3.setAccessPoint_RSSI(scanResult.level);
                        accessPoint3.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }
                }else if (scanResult.SSID.contains("TP-LINK_6E9CB4")){
                    if (accessPoint4.getAccessPoint_RSSI()!=0){
                        accessPoint4.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint4.setAccessPoint_RSSI(0.5*accessPoint4.getAccessPoint_RSSI()+(0.5)*scanResult.level);
                        accessPoint4.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }else{
                        accessPoint4.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint4.setAccessPoint_RSSI(scanResult.level);
                        accessPoint4.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }
                }else if (scanResult.SSID.contains("TP-LINK_6E88D8")){
                    if (accessPoint5.getAccessPoint_RSSI()!=0){
                        accessPoint5.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint5.setAccessPoint_RSSI(0.5*accessPoint5.getAccessPoint_RSSI()+(0.5)*scanResult.level);
                        accessPoint5.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }else{
                        accessPoint5.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint5.setAccessPoint_RSSI(scanResult.level);
                        accessPoint5.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }
                }else if (scanResult.SSID.contains("TP-LINK_6E894A")){
                    if (accessPoint6.getAccessPoint_RSSI()!=0){
                        accessPoint6.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint6.setAccessPoint_RSSI(0.5*accessPoint5.getAccessPoint_RSSI()+(0.5)*scanResult.level);
                        accessPoint6.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }else{
                        accessPoint6.setAccessPoint_SSID(scanResult.SSID);
                        accessPoint6.setAccessPoint_RSSI(scanResult.level);
                        accessPoint6.setAccessPoint_FREQUENCY(scanResult.frequency);
                    }
                }
            }
        }

    }

    public ArrayList<AccessPoint> getAccessPoints(){
        accessPointList.clear();
        if (accessPoint1.getAccessPoint_RSSI()!=0){
            accessPointList.add(accessPoint1);
        }

        if (accessPoint2.getAccessPoint_RSSI()!=0){
            accessPointList.add(accessPoint2);
        }

        if (accessPoint3.getAccessPoint_RSSI()!=0){
            accessPointList.add(accessPoint3);
        }

        if (accessPoint4.getAccessPoint_RSSI()!=0){
            accessPointList.add(accessPoint4);
        }

        if (accessPoint5.getAccessPoint_RSSI()!=0){
            accessPointList.add(accessPoint5);
        }

        if (accessPoint6.getAccessPoint_RSSI()!=0){
            accessPointList.add(accessPoint6);
        }

        return accessPointList;
    }

}
