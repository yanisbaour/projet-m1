package com.example.wytmproject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Observable;

public class BusinessLayer extends Observable {

    private  PrintWriter flux_sortie = null ;
    private  BufferedReader flux_entree = null ;
    Connection connection = new Connection();

    public BusinessLayer() {}





    //Authentification
    public String checkForAuthentification(String login, String password)
    {
        try {
            connection.getConnection();
            String reply;
            connection.getFlux_sortie().println ("authentification") ;
            connection.getFlux_sortie().println (login) ;
            connection.getFlux_sortie().println (password) ;
            reply =  connection.getFlux_entree().readLine() ;
            return reply;
        } catch (IOException e) {
            System.out.println("erreur !!");
            return "";
        }
    }

    //Inscription
    public String inscription(String login,String email, String password)
    {
        try {
            connection.getConnection();
            String reply;
            connection.getFlux_sortie().println ("inscription") ;
            connection.getFlux_sortie().println (login) ;
            connection.getFlux_sortie().println (email) ;
            connection.getFlux_sortie().println (password) ;
            reply =  connection.getFlux_entree().readLine() ;
            return reply;
        } catch (IOException e) {
            System.out.println("erreur !!");
            return "";
        }
    }

    //locate
    public String localisation(String sSSID1,String sSSID2,String sSSID3,double distance1,double distance2,double distance3, String login)
    {
        try {
            String reply;
            //double distance1,distance2,distance3;
            PositionUser positionUser = new PositionUser();

            connection.getConnection();
            connection.getFlux_sortie().println ("localisation");

            /*distance1 = positionUser.calculateDistanceFromUserToAccessPoint(accessPoint1.getAccessPoint_FREQUENCY(),accessPoint1.getAccessPoint_RSSI());//accessPoint1.getRSSI()...
            distance2 = positionUser.calculateDistanceFromUserToAccessPoint(accessPoint2.getAccessPoint_FREQUENCY(),accessPoint2.getAccessPoint_RSSI());
            distance3 = positionUser.calculateDistanceFromUserToAccessPoint(accessPoint3.getAccessPoint_FREQUENCY(),accessPoint3.getAccessPoint_RSSI());
*/

            positionUser = positionUser.calculatePositionUserCoordonates(sSSID1,sSSID2,sSSID3,distance1,distance2,distance3);

            connection.getFlux_sortie().println(distance1);
            connection.getFlux_sortie().println(distance2);
            connection.getFlux_sortie().println(distance3);
            connection.getFlux_sortie().println(positionUser.getX());
            connection.getFlux_sortie().println(positionUser.getY());
            connection.getFlux_sortie().println(login);

            reply =  connection.getFlux_entree().readLine();
            return reply;
        } catch (IOException e) {
            System.out.println("erreur !!");
            return "";
        }
    }

    //locate
    public void sendPosition(double x,double y, String login)
    {
        try {
            connection.getConnection();
            connection.getFlux_sortie().println ("positionUser");

            connection.getFlux_sortie().println(x);
            connection.getFlux_sortie().println(y);
            connection.getFlux_sortie().println(login);

        } catch (IOException e) {
            System.out.println("erreur !!");
        }
    }

    //destination
    public String[] getPlaces()
    {
        try {
            String[] placeArray;
            String places;
            connection.getConnection();
            connection.getFlux_sortie().println ("places") ;
            places = connection.getFlux_entree().readLine() ;
            placeArray = places.split(";");
            return placeArray;
        } catch (IOException e) {
            System.out.println("erreur !!");
            return null;
        }
    }

    //get destinations Intintelligently
    public String[] getPlacesIntintelligently(String user, String place)
    {
        try {
            //AccessPoint accessPoint1 = null,accessPoint2 = null ,accessPoint3 = null ;

            String[] placeArray;
            String places;

            connection.getConnection();
            connection.getFlux_sortie().println ("placesIntintelligently") ;

            //place = localisation(accessPoint1,accessPoint2,accessPoint3,user);

            connection.getFlux_sortie().println (user) ;
            connection.getFlux_sortie().println ("salle A462") ;// probleme : comment avoir la salle ou il est
            places = connection.getFlux_entree().readLine() ;
            placeArray = places.split(";");
            return placeArray;
        } catch (IOException e) {
            System.out.println("erreur !!");
            return null;
        }
    }

    //close connection

    public void notifyObservers(Object arg) {
        super.setChanged();
        super.notifyObservers(arg);
    }

    /** Close the socket */
    public void closeConnection() {
        Socket socket = connection.getSocket();
        try {
            socket.close();
        } catch (IOException ex) {
            notifyObservers(ex);
        }
    }

}
