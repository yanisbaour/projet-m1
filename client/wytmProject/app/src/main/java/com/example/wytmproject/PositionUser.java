package com.example.wytmproject;

import android.os.Build;
import android.support.annotation.RequiresApi;

import java.time.LocalTime;

public class PositionUser {
    private double x;
    private double y;
    private LocalTime time;
    final AccessPoint TP_LINK_6E894A = new AccessPoint(25.26866667, 10.6653333);//zinedine
    final AccessPoint TP_LINK_6E9CB2 = new AccessPoint(0.3, 3.0);//salle A458 pas mesurée exactement :: RSSI a 1 m = -22
    final AccessPoint TP_LINK_6E896C = new AccessPoint(16.60133333, 0.374);// salle IE ::: RRSSI a 1 m : -24
    final AccessPoint TP_LINK_6E88D8 = new AccessPoint(38.82133333, 11.5086667);//derdouri
    final AccessPoint TP_LINK_6E9CB4 = new AccessPoint(36.055, 6.66266667); //A498 ::: RRSSI a 1 m : -24
    final AccessPoint TP_LINK_44D2C4 = new AccessPoint(12.14666667, 10.658);//A463 //mayumi

    public PositionUser() {}

    public PositionUser(double x, double y, LocalTime time) {
        super();
        this.x = x;
        this.y = y;
        this.time = time;
    }

    public double calculateDistanceFromUserToBeacon(double rSSI){
        double distance;
        float mP=-30;
        distance=Math.pow(10,((mP-rSSI)/(Math.pow(10,2)))) /* *(11/15) */;
        return distance;
    }

    public double  calculateDistanceFromUserToAccessPoint(double frequency, double signalLevel, String ap_SSID){
        //System.out.println(" frequency : "+frequency + " RSSI : "+signalLevel );
        double distance, signalLevel_1m = 0, n;

        switch(ap_SSID) {
            case "TP-LINK_6E894A":
                signalLevel_1m=-23.5;//24
                n=24;
                break;
            case "TP-LINK_6E9CB4": //
                signalLevel_1m=-33;// fini
                n=22;
                break;
            case "TP-LINK_6E9CB2":
                signalLevel_1m=-32.5;//30
                n=22;
                break;
            case "TP-LINK_6E896C":
                signalLevel_1m=-35.5;//30
                n=22;
                break;
            case "TP-LINK_6E88D8":
                signalLevel_1m=-31;//30
                n=22.5;
                break;
            case "TP-LINK_44D2C4":
                signalLevel_1m=-26.3;//30
                n=22;
                break;
            default:
                signalLevel_1m=-24;//30
                n=22;
                break;
        }

        distance = Math.pow(10,( (signalLevel_1m - signalLevel)/(n)))*(float)((double)11/(double)15);

       // double distance;
        //distance = Math.pow(10,(((27.55-(20*Math.log10(frequency))+Math.abs(signalLevel))/20)));
        return distance;
    }

    public PositionUser calculatePositionUserCoordonates (String sSID1,String sSID2,String sSID3,double distance1, double distance2, double distance3){
        double temp1;
        double temp2;

        double x1=0,x2=0,x3=0,x;
        double y1=0,y2=0,y3=0,y;

        switch(sSID1) {
            case "TP-LINK_6E894A":
                x1=TP_LINK_6E894A.getX();
                y1=TP_LINK_6E894A.getY();
                break;
            case "TP-LINK_6E9CB4":
                x1=TP_LINK_6E9CB4.getX();
                y1=TP_LINK_6E9CB4.getY();
                break;
            case "TP-LINK_6E9CB2":
                x1=TP_LINK_6E9CB2.getX();
                y1=TP_LINK_6E9CB2.getY();
                break;
            case "TP-LINK_6E896C":
                x1=TP_LINK_6E896C.getX();
                y1=TP_LINK_6E896C.getY();
                break;
            case "TP-LINK_6E88D8":
                x1=TP_LINK_6E88D8.getX();
                y1=TP_LINK_6E88D8.getY();
                break;
            case "TP-LINK_44D2C4":
                x1=TP_LINK_6E896C.getX();
                y1=TP_LINK_6E896C.getY();
                break;
            default:
                break;
        }

        switch(sSID2) {
            case "TP-LINK_6E894A":
                 x2=TP_LINK_6E894A.getX();
                 y2=TP_LINK_6E894A.getY();
                break;
            case "TP-LINK_6E9CB4":
                 x2=TP_LINK_6E9CB4.getX();
                 y2=TP_LINK_6E9CB4.getY();
                break;
            case "TP-LINK_6E9CB2":
                 x2=TP_LINK_6E9CB2.getX();
                 y2=TP_LINK_6E9CB2.getY();
                break;
            case "TP-LINK_6E896C":
                 x2=TP_LINK_6E896C.getX();
                 y2=TP_LINK_6E896C.getY();
                break;
            case "TP-LINK_6E88D8":
                 x2=TP_LINK_6E88D8.getX();
                 y2=TP_LINK_6E88D8.getY();
                break;
            case "TP-LINK_44D2C4":
                 x2=TP_LINK_44D2C4.getX();
                 y2=TP_LINK_44D2C4.getY();
                break;
            default:
                break;
        }

        switch(sSID3) {
            case "TP-LINK_6E894A":
                 x3=TP_LINK_6E894A.getX();
                 y3=TP_LINK_6E894A.getY();
                break;
            case "TP-LINK_6E9CB4":
                 x3=TP_LINK_6E9CB4.getX();
                 y3=TP_LINK_6E9CB4.getY();
                break;
            case "TP-LINK_6E9CB2":
                 x3=TP_LINK_6E9CB2.getX();
                 y3=TP_LINK_6E9CB2.getY();
                break;
            case "TP-LINK_6E896C":
                 x3=TP_LINK_6E896C.getX();
                 y3=TP_LINK_6E896C.getY();
                break;
            case "TP-LINK_6E88D8":
                 x3=TP_LINK_6E88D8.getX();
                 y3=TP_LINK_6E88D8.getY();
                break;
            case "TP-LINK_44D2C4":
                 x3=TP_LINK_44D2C4.getX();
                 y3=TP_LINK_44D2C4.getY();
                break;
            default:
                break;
        }


        temp1 = ((Math.pow(x3,2)) - (Math.pow(x2,2)) + (Math.pow(y3,2)) - (Math.pow(y2,2)) + (Math.pow(distance2,2)) - (Math.pow(distance3,2))) /2;

        temp2 = ((Math.pow(x1,2)) - (Math.pow(x2,2)) + (Math.pow(y1,2)) - (Math.pow(y2,2)) + (Math.pow(distance2,2)) - (Math.pow(distance1,2))) /2;

        y = ((temp2*(x2-x3)) - (temp1*(x2-x1))) / ( ((y1-y2) * (x2-x3)) -  ((y3-y2)*(x2-x1)));

        x = ((y * (y1-y2)) - temp2) / (x2-x1);

        //LocalTime time = LocalTime.now();

        PositionUser user = new PositionUser();

        user.setX(x);
        user.setY(y);

        return user;
    }

    public PositionUser calculatePositionUserCoordonatesTest (Point2D P1, Point2D P2, Point2D P3,double distance1, double distance2, double distance3){
        double temp1;
        double temp2;

        double x,x1,x2,x3;
        double y,y1,y2,y3;

        x1=P1.getX();
        x2=P2.getX();
        x3=P3.getX();

        y1=P1.getY();
        y2=P2.getY();
        y3=P3.getY();


        temp1 = ((Math.pow(x3,2)) - (Math.pow(x2,2)) + (Math.pow(y3,2)) - (Math.pow(y2,2)) + (Math.pow(distance2,2)) - (Math.pow(distance3,2))) /2;

        temp2 = ((Math.pow(x1,2)) - (Math.pow(x2,2)) + (Math.pow(y1,2)) - (Math.pow(y2,2)) + (Math.pow(distance2,2)) - (Math.pow(distance1,2))) /2;

        y = ((temp2*(x2-x3)) - (temp1*(x2-x1))) / ( ((y1-y2) * (x2-x3)) -  ((y3-y2)*(x2-x1)));

        x = ((y * (y1-y2)) - temp2) / (x2-x1);

        //LocalTime time = LocalTime.now();

        PositionUser user = new PositionUser();

        user.setX(x);
        user.setY(y);

        return user;
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }
}
