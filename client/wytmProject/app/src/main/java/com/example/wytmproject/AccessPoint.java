package com.example.wytmproject;

public class AccessPoint {
    private String accessPoint_SSID;
    private double accessPoint_RSSI;
    private int accessPoint_FREQUENCY;
    private double x;
    private double y;

    public AccessPoint(){}

    public AccessPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public AccessPoint(int accessPoint_RSSI, int accessPoint_FREQUENCY) {
        this.accessPoint_RSSI = accessPoint_RSSI;
        this.accessPoint_FREQUENCY = accessPoint_FREQUENCY;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public int getAccessPoint_FREQUENCY() {
        return accessPoint_FREQUENCY;
    }

    public void setAccessPoint_FREQUENCY(int accessPoint_FREQUENCY) {
        this.accessPoint_FREQUENCY = accessPoint_FREQUENCY;
    }

    public void AccessPoint(String accessPoint_SSID, double accessPoint_RSSI){
        this.accessPoint_SSID = accessPoint_SSID;
        this.accessPoint_RSSI = accessPoint_RSSI;
    }
    public String getAccessPoint_SSID() {return accessPoint_SSID; }

    public void setAccessPoint_SSID(String accessPoint_SSID) {
        this.accessPoint_SSID = accessPoint_SSID;
    }
    public double getAccessPoint_RSSI() {
        return accessPoint_RSSI;
    }

    public void setAccessPoint_RSSI(double accessPoint_RSSI) {
        this.accessPoint_RSSI = accessPoint_RSSI;
    }

}