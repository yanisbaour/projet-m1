package com.example.wytmproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DestinationListe extends AppCompatActivity {
    ListView listView;
    TextView textView;
    String[] listItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        String login ;

        Intent intent = getIntent();
        login =  intent.getStringExtra("login");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination_liste);

        BusinessLayer businessLayer = new BusinessLayer();

        listView = (ListView) findViewById(R.id.listView);
        textView = (TextView) findViewById(R.id.textView);
        listItem = businessLayer.getPlacesIntintelligently(login,"place");//getResources().getStringArray(R.array.array_technology);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, listItem);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value = adapter.getItem(position);
                //Toast.makeText(getApplicationContext(),value,Toast.LENGTH_SHORT).show();

                Intent i5 = new Intent().setClass(DestinationListe.this, Carte.class);
                startActivity(i5);

            }
        });
    }
}

