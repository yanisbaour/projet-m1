package com.example.wytmproject;

public class Point2D {
    private double x;
    private double y;

    public Point2D(double x , double y) {
        super();
        this.x = x;
        this.y = y;
    }

    public Point2D() {
    }
    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
