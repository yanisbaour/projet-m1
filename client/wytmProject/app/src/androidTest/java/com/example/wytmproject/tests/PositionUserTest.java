package com.example.wytmproject.tests;
import android.net.wifi.WifiManager;

import com.example.wytmproject.AccessPoint;
import com.example.wytmproject.Point2D;
import com.example.wytmproject.PositionUser;
import com.example.wytmproject.WifiReceiver;

import org.junit.Test;


import static org.junit.Assert.assertEquals;

public class PositionUserTest {
    /*
    @Test
    public void calculateDistanceFromUserToAccessPoint() {
        PositionUser positionUser = new PositionUser(4, 3, null);


        assertEquals(0.314,positionUser.calculateDistanceFromUserToAccessPoint(2400,-30),0.2);
    }*/

    @Test
    public void calculatePositionUserCoordonates() {

        Point2D P1 = new Point2D(0.3, 3);
        Point2D P2 = new Point2D(16.60133333, 0.374);
        Point2D P3 = new Point2D(12.14666667, 10.658);

        PositionUser Pu = new PositionUser(4, 3, null);

        double distance1 = Math.sqrt(Math.pow((Pu.getX() - P1.getX()), 2) + Math.pow((Pu.getY() - P1.getY()), 2));
        double distance2 = Math.sqrt(Math.pow((Pu.getX() - P2.getX()), 2) + Math.pow((Pu.getY() - P2.getY()), 2));
        double distance3 = Math.sqrt(Math.pow((Pu.getX() - P3.getX()), 2) + Math.pow((Pu.getY() - P3.getY()), 2));

        PositionUser PUTest= new PositionUser(0,0,null);

        PUTest=PUTest.calculatePositionUserCoordonatesTest(P1,P2,P3,distance1,distance2,distance3);

        if (Pu.getX()==PUTest.getX()){
            System.out.println("Correct X");
        }
        else {
            System.out.println("Expected X:");
            System.out.println(Pu.getX());
            System.out.println("Calculated X:");
            System.out.println(PUTest.getX());
        }
        if (Pu.getY()==PUTest.getY()){
            System.out.println("CorrectY");
        }
        else {
            System.out.println("Expected Y:");
            System.out.println(Pu.getY());
            System.out.println("Calculated Y:");
            System.out.println(PUTest.getY());
        }

        assertEquals(Pu.getX(),PUTest.getX(),0.00000001 );
        assertEquals(Pu.getY(),PUTest.getY(),0.00000001 );


    }


}
