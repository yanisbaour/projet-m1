package com.example.wytmproject.tests;

import com.example.wytmproject.AccessPoint;
import com.example.wytmproject.BusinessLayer;
import com.example.wytmproject.Connection;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class BusinessLayerTest {


    @org.junit.Test
    public void checkForAuthentification() {
        BusinessLayer businessLayer = new BusinessLayer();
        assertEquals("succes",businessLayer.checkForAuthentification("mrabin","pwdrabin1"));
    }

    @org.junit.Test
    public void inscription() {
        BusinessLayer businessLayer = new BusinessLayer();
        //assertEquals("succes",businessLayer.inscription("test","test@gmail.com","test"));
        assertEquals("echec : exists",businessLayer.inscription("mrabin","test@gmail.com","test"));
    }

    @org.junit.Test
    public void getPlaces() throws IOException {
        BusinessLayer businessLayer = new BusinessLayer();
        String[] places;
        places = businessLayer.getPlaces();

        assertEquals(10,places.length);
    }
/*
    @org.junit.Test
    public void localisation() throws IOException {

        BusinessLayer businessLayer = new BusinessLayer();
        AccessPoint accessPoint1 = new AccessPoint();
        AccessPoint accessPoint2 = new AccessPoint();
        AccessPoint accessPoint3 = new AccessPoint();

        accessPoint1.setAccessPoint_SSID("TP-LINK_6E9CB2");
        accessPoint2.setAccessPoint_SSID("TP-LINK_6E9CB4");
        accessPoint3.setAccessPoint_SSID("TP-LINK_6E894A");

        accessPoint1.setAccessPoint_RSSI(-58);
        accessPoint2.setAccessPoint_RSSI(-44);
        accessPoint3.setAccessPoint_RSSI(-49);

        accessPoint1.setAccessPoint_FREQUENCY(2050);
        accessPoint2.setAccessPoint_FREQUENCY(2400);
        accessPoint3.setAccessPoint_FREQUENCY(2400);

        assertEquals("salle A462",businessLayer.localisation(accessPoint1,accessPoint2,accessPoint3,"test"));
    }*/
/*
    @org.junit.Test
    public void closeConnection() throws IOException {
        Connection connection = new Connection();
        connection.closeConnection();
    }
*/
    @org.junit.Test
    public void sendPosition() throws IOException {
        BusinessLayer businessLayer = new BusinessLayer();
        businessLayer.sendPosition(1,2,"test");
    }
/*
    @org.junit.Test
    public void getPlacesIntintelligently() throws IOException {
        BusinessLayer businessLayer = new BusinessLayer();
        String[] visites;
        visites = businessLayer.getPlacesIntintelligently("test");

        for (int i=0; i< visites.length;i++){
            System.out.println(visites[i]);
        }


        assertEquals(9,visites.length);
    }*/
}